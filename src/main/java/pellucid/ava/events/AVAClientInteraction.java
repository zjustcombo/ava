package pellucid.ava.events;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Binocular;
import pellucid.ava.items.miscs.MeleeWeapon;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.packets.*;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class AVAClientInteraction extends AVAClientEvent
{
    @SubscribeEvent
    public static void onClientTick(TickEvent.ClientTickEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player == null || !player.isAlive() || event.phase == TickEvent.Phase.START)
            return;
        boolean focused = minecraft.isGameFocused() && minecraft.currentScreen == null;
        if (isFullEquipped(player))
        {
            IPlayerAction capability = cap(player);
            int uav = capability.getUAVDuration();
            if (uav == 139 || uav == 134 || uav == 119 || uav == 114 || uav == 85 || uav == 80 || uav == 65 || uav == 60)
                player.playSound(AVASounds.UAV_CAPTURED, 1.0F, 1.0F);
        }
        IPlayerAction capability = cap(player);
        capability.getHurtInfo().update();
        if (capability.getFlashDuration() > 0)
            capability.setFlashDuration(capability.getFlashDuration() - 1);
        if (capability.getkillTipCooldown() > 0 && !capability.getKillTips().getTips().isEmpty())
            capability.setkillTipCooldown(capability.getkillTipCooldown() - 1);
        if (capability.getkillTipCooldown() == 0)
            if (!capability.getKillTips().getTips().isEmpty())
            {
                capability.removeKillTip();
                capability.setkillTipCooldown(100);
            }
        if (capability.getKillTips().getTips().isEmpty())
            capability.setkillTipCooldown(100);
        int battery = capability.getNightVisionBattery();
        if (capability.isNightVisionActivated())
        {
            if (!player.isCreative())
                capability.setNightVisionBattery(battery - 1);
            if (capability.getNightVisionBattery() < 1 || !isFullEquipped(player) || !focused || player.isSpectator())
                capability.setNightVisionActive(false);
        }
        else if (battery < 600)
            capability.setNightVisionBattery(Math.min(600, battery + 3));
        if (isAvailable(player))
        {
            if (focused)
            {
                while (ClientModEventBus.RELOAD.isPressed())
                    reload(player, getHeldStack(player));
                while (ClientModEventBus.NIGHT_VISION_DEVICE_SWITCH.isPressed() && isFullEquipped(player))
                {
                    capability.setNightVisionActive(!capability.isNightVisionActivated() && capability.getNightVisionBattery() > 0);
                    if (capability.isNightVisionActivated())
                        playSound(AVASounds.NIGHT_VISION_ACTIVATE);
                }
                if (GLFW.glfwGetMouseButton(minecraft.getMainWindow().getHandle(), 0) == 1 && getGun(player).isAuto())
                    fire(player, getHeldStack(player));
                else if (capability.isFiring())
                    pauseFire(capability);
            }
            else if (capability.isAiming())
                setAiming(capability, false);
        }
        ItemStack stack = getHeldStack(player);
        if (stack.getItem() instanceof MeleeWeapon)
        {
            MeleeWeapon knife = (MeleeWeapon) stack.getItem();
            if (knife.canMelee(stack))
            {
                int p = 0;
                if (GLFW.glfwGetMouseButton(minecraft.getMainWindow().getHandle(), 0) == 1)
                    p = 1;
                else if (GLFW.glfwGetMouseButton(minecraft.getMainWindow().getHandle(), 1) == 1)
                    p = 2;
                if (p != 0)
                {
                    knife.preMelee(stack, p == 1);
                    AVAPackets.INSTANCE.sendToServer(new AttemptToMeleeMessage(p == 1));
                }
            }
        }
        if (!(stack.getItem() instanceof AVAItemGun))
        {
            if (capability.isFiring())
                pauseFire(capability);
            if (capability.isAiming())
                setAiming(capability, false);
        }
        if (stack.getItem() instanceof Binocular && !focused)
        {
            if (((Binocular) stack.getItem()).initTags(stack).getBoolean("aiming"))
            {
                AVAPackets.INSTANCE.sendToServer(new BinocularAimMessage(false));
                ((Binocular) stack.getItem()).initTags(stack).putBoolean("aiming", false);
            }
        }
    }

    @SubscribeEvent
    public static void attempToMeleeAttack(AttackEntityEvent event)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (isAvailable(player)) event.setCanceled(true);
    }

    @SubscribeEvent
    public static void onMouseClick(InputEvent.MouseInputEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player == null || !player.isAlive())
            return;
        if (event.getAction() == 1 && isAvailable(player) && minecraft.isGameFocused() && minecraft.currentScreen == null)
        {
            if (event.getButton() == 1)
            {
                IPlayerAction capability = cap(player);
                if ((capability.isAiming() || ableToAim(player)))
                {
                    boolean interactable = getGun(player).isReloadInteractable();
                    CompoundNBT compound =  getGun(player).initTags(getHeldStack(player));
                    if (compound.getInt("reload") == 0 || interactable)
                    {
                        if (capability.isAiming())
                            compound.putInt("interact", 1);
                        setAiming(capability, !capability.isAiming());
                    }
                }
            }
            else if (event.getButton() == 0)
                if (!getGun(player).isAuto())
                    fire(player, getHeldStack(player));
        }
        if (minecraft.isGameFocused() && minecraft.currentScreen == null)
        {
            ItemStack stack = getHeldStack(player);
            if (stack.getItem() instanceof ThrowableItem)
                AVAPackets.INSTANCE.sendToServer(new ThrowGrenadeMessage(event.getAction() == 0 ? event.getButton() : 2));
            else if (stack.getItem() instanceof Binocular && event.getAction() == 1)
            {
                boolean aiming = ((Binocular) stack.getItem()).initTags(stack).getBoolean("aiming");
                AVAPackets.INSTANCE.sendToServer(event.getButton() == 1 ? new BinocularAimMessage(!aiming) : new BinocularUseMessage());
            }
        }
    }

    @SubscribeEvent
    public static void onPlayerLeftClickBlock(PlayerInteractEvent.LeftClickBlock event)
    {
        Item item = event.getPlayer().getHeldItemMainhand().getItem();
        if (item instanceof AVAItemGun || item instanceof Binocular || item instanceof MeleeWeapon)
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void modifyToolTips(ItemTooltipEvent event)
    {
        ItemStack stack = event.getItemStack();
        Item item = stack.getItem();
        boolean modify = false;
        if (item instanceof AVAItemGun)
        {
            event.getToolTip().clear();
            AVAItemGun gun = ((AVAItemGun) item);
            CompoundNBT compound = gun.initTags(stack);
            event.getToolTip().add(new TranslationTextComponent(item.getTranslationKey()));
            event.getToolTip().add(new StringTextComponent("Ammo: " + compound.getInt("ammo") + "/" + gun.getMaxAmmo()));
            modify = true;
        }
        if (item instanceof Binocular)
        {
            event.getToolTip().clear();
            event.getToolTip().add(new TranslationTextComponent(item.getTranslationKey()));
            modify = true;
        }
        if (modify && event.getFlags().isAdvanced())
            event.getToolTip().add((new StringTextComponent(ForgeRegistries.ITEMS.getKey(item).toString())).setStyle(new Style().setColor(TextFormatting.DARK_GRAY)));
    }
}
