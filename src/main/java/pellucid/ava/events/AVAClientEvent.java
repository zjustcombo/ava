package pellucid.ava.events;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVASingle;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;
import pellucid.ava.misc.packets.*;
import pellucid.ava.misc.renderers.AVARenderer;

public class AVAClientEvent extends AVACommonEvent
{
    protected static boolean isAiming(PlayerEntity player)
    {
        return cap(player).isAiming() && ableToAim(player);
    }

    protected static boolean ableToAim(PlayerEntity player)
    {
        AVAItemGun gun = getGun(player);
        CompoundNBT compound = gun.initTags(getHeldStack(player));
        if (((!(gun instanceof AVASingle)) || (compound.getInt("fire") == 0)) && gun.getScopeType() != AVAItemGun.ScopeTypes.NONE)
            return true;
        else if (cap(player).isAiming())
        {
            cap(player).setAiming(false);
            AVAPackets.INSTANCE.sendToServer(new AimMessage(false, true));
        }
        return false;
    }

    protected static void fire(PlayerEntity player, ItemStack stack)
    {
        AVAItemGun gun = getGun(player);
        IPlayerAction capability = cap(player);
        if (gun.firable(player, stack))
        {
            CompoundNBT compound = gun.initTags(stack);
            if (gun.firable(player, stack))
            {
                gun.fire(player.getEntityWorld(), player, stack);
                capability.setFiring(true);
            }
            else if (compound.getInt("ticks") == 0)
                capability.setFiring(false);
            if (compound.getInt("ammo") < 1 && gun.reloadable(player, stack))
                gun.preReload(player, stack);
        }
        AVAPackets.INSTANCE.sendToServer(new AttemptToFireMessage());
    }

    public static boolean isSamePlayer(PlayerEntity player, PlayerEntity player2)
    {
        return player.getEntityId() == player2.getEntityId();
    }

    protected static void pauseFire(IPlayerAction capability)
    {
        capability.setFiring(false);
        AVAPackets.INSTANCE.sendToServer(new FiringMessage(false, true));
    }

    protected static void reload(PlayerEntity player, ItemStack stack)
    {
        AVAItemGun gun = (AVAItemGun) stack.getItem();
        if (gun.reloadable(player, stack))
        {
            gun.preReload(player, stack);
            PlayerAction.getCap(player).setAiming(false);
        }
        AVAPackets.INSTANCE.sendToServer(AttemptToReloadMessage.create());
    }

    protected static void setAiming(IPlayerAction capability, boolean aiming)
    {
        capability.setAiming(aiming);
        AVAPackets.INSTANCE.sendToServer(new AimMessage(aiming, true));
    }

    protected static int getColourForName(PlayerEntity target, PlayerEntity local)
    {
        return self(target, local) ? 16764928 : isOnSameTeam(target, local, false) ? 17663 : 16711680;
    }

    protected static boolean isOnSameTeam(PlayerEntity player, PlayerEntity player2, boolean selfInc)
    {
        return player.getTeam() != null && player.getWorldScoreboard().getTeam(player.getTeam().getName()).getMembershipCollection().contains(player2.getScoreboardName()) && (selfInc || !self(player, player2));
    }

    protected static boolean self(PlayerEntity player, PlayerEntity player2)
    {
        return player.getEntityId() == player2.getEntityId();
    }

    protected static void playSound(SoundEvent sound)
    {
        if (sound.getRegistryName() == null)
            return;
        AVAPackets.INSTANCE.sendToServer(new PlaySoundMessage(sound.getRegistryName().toString()));
    }

    @OnlyIn(Dist.CLIENT)
    protected static void drawTransparent(boolean enable)
    {
        if (enable)
        {
            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();
        }
        else
        {
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            RenderSystem.disableBlend();
        }
    }

    @OnlyIn(Dist.CLIENT)
    protected static void fillScreen(int screenWidth, int screenHeight)
    {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(0.0D, screenHeight, -90.0D).tex(0.0F, 1.0F).endVertex();
        bufferbuilder.pos(screenWidth, screenHeight, -90.0D).tex(1.0F, 1.0F).endVertex();
        bufferbuilder.pos(screenWidth, 0.0D, -90.0D).tex(1.0F, 0.0F).endVertex();
        bufferbuilder.pos(0.0D, 0.0D, -90.0D).tex(0.0F, 0.0F).endVertex();
        tessellator.draw();
    }

    @OnlyIn(Dist.CLIENT)
    protected static void fillCentredMaxWithFilledSides(int screenWidth, int screenHeight, ResourceLocation image)
    {
        fillCentredMax(screenWidth, screenHeight, image);
        fillBlankSides(screenWidth, screenHeight);
    }

    @OnlyIn(Dist.CLIENT)
    protected static void fillCentredMax(int screenWidth, int screenHeight, ResourceLocation image)
    {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        int w = (screenWidth - screenHeight) / 2;
        int p = screenWidth - w;
        Minecraft.getInstance().getTextureManager().bindTexture(image);
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(w, screenHeight, -90.0D).tex(0.0F, 1.0F).endVertex();
        bufferbuilder.pos(p, screenHeight, -90.0D).tex(1.0F, 1.0F).endVertex();
        bufferbuilder.pos(p, 0.0D, -90.0D).tex(1.0F, 0.0F).endVertex();
        bufferbuilder.pos(w, 0.0D, -90.0D).tex(0.0F, 0.0F).endVertex();
        tessellator.draw();
    }

    @OnlyIn(Dist.CLIENT)
    private static void fillBlankSides(int screenWidth, int screenHeight)
    {
        Minecraft.getInstance().getTextureManager().bindTexture(AVARenderer.BLACK);
        int w = (screenWidth - screenHeight) / 2;
        int p = screenWidth - w;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(0.0D, screenHeight, -90.0D).tex(0.0F, 1.0F).endVertex();
        bufferbuilder.pos(w, screenHeight, -90.0D).tex(1.0F, 1.0F).endVertex();
        bufferbuilder.pos(w, 0.0D, -90.0D).tex(1.0F, 0.0F).endVertex();
        bufferbuilder.pos(0.0D, 0.0D, -90.0D).tex(0.0F, 0.0F).endVertex();
        tessellator.draw();
        bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(p, screenHeight, -90.0D).tex(0.0F, 1.0F).endVertex();
        bufferbuilder.pos(screenWidth, screenHeight, -90.0D).tex(1.0F, 1.0F).endVertex();
        bufferbuilder.pos(screenWidth, 0.0D, -90.0D).tex(1.0F, 0.0F).endVertex();
        bufferbuilder.pos(p, 0.0D, -90.0D).tex(0.0F, 0.0F).endVertex();
        tessellator.draw();
    }

    public static void drawCenteredString(FontRenderer font, String text, int x, int y, int colour)
    {
        font.drawStringWithShadow(text, (float)(x - font.getStringWidth(text) / 2), (float)y, colour);
    }
}
