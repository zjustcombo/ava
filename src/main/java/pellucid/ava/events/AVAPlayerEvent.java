package pellucid.ava.events;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.NetworkDirection;
import pellucid.ava.AVA;
import pellucid.ava.entities.scanhits.BinocularRTBulletEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Binocular;
import pellucid.ava.misc.AVADamageSource;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.KillTips;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.commands.RecoilRefundTypeCommand;
import pellucid.ava.misc.config.AVAServerConfig;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.SyncCapabilityDataMessage;

import java.util.Objects;

@Mod.EventBusSubscriber(modid = AVA.MODID)
public class AVAPlayerEvent extends AVACommonEvent
{
    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event)
    {
        if (event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().getEntityWorld().isRemote())
            syncWorldCapWithClient((ServerPlayerEntity) event.getPlayer());
    }

    @SubscribeEvent
    public static void onPlayerChangedDimension(PlayerEvent.PlayerChangedDimensionEvent event)
    {
        if (event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().getEntityWorld().isRemote())
            syncWorldCapWithClient((ServerPlayerEntity) event.getPlayer());
    }

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent event)
    {
        if (event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().getEntityWorld().isRemote())
            syncWorldCapWithClient((ServerPlayerEntity) event.getPlayer());
    }

    @SubscribeEvent
    public static void onPlayerClone(PlayerEvent.Clone event)
    {
        IPlayerAction oldCap = PlayerAction.getCap(event.getOriginal());
        IPlayerAction newCap = PlayerAction.getCap(event.getPlayer());
        newCap.setAttackDamageBoost(oldCap.getAttackDamageBoost());
        newCap.setHealthBoost(oldCap.getHealthBoost());
    }

    @SubscribeEvent
    public static void onArmourChanged(LivingEquipmentChangeEvent event)
    {
        LivingEntity entity = (LivingEntity) event.getEntity();
        if (entity instanceof PlayerEntity)
        {
            IAttributeInstance knockbackResistance = entity.getAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE);
            boolean hasModifier = knockbackResistance.hasModifier(KNOCKBACK_RESISTANCE_MODIFIER);
            if (isFullEquipped((PlayerEntity) entity))
            {
                if (!hasModifier)
                    knockbackResistance.applyModifier(KNOCKBACK_RESISTANCE_MODIFIER);
            }
            else if (hasModifier) knockbackResistance.removeModifier(KNOCKBACK_RESISTANCE_MODIFIER);
        }
    }

    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent event)
    {
        PlayerEntity player = event.player;
        if (event.phase == TickEvent.Phase.START || player == null || !player.isAlive())
            return;
        ItemStack stack = getHeldStack(player);
        if (isAvailable(player))
        {
            IPlayerAction capability = cap(player);
            if (capability.isAiming() || capability.isFiring() || getGun(player).initTags(getHeldStack(player)).getInt("reload") != 0)
                if (player.isSprinting())
                    player.setSprinting(false);
        }
        IPlayerAction capability = cap(player);
        World world = player.world;
        if (isFullEquipped(player) && !world.isRemote)
        {
            capability.setStepSoundCooldown(capability.getStepSoundCooldown() - 1);
            if (isMovingOnGroundServer(player) && !player.isSneaking() && capability.getStepSoundCooldown() <= 0)
            {
                SoundEvent sound = player.world.getBlockState(new BlockPos(MathHelper.floor(player.getPosX()), MathHelper.floor(player.getPosY() - 0.2F), MathHelper.floor(player.getPosZ()))).getSoundType().getStepSound();
                if (sound == SoundEvents.BLOCK_WOOD_STEP)
                    sound = AVASounds.FOOTSTEP_WOOD;
                else if (sound == SoundEvents.BLOCK_METAL_STEP || sound == SoundEvents.BLOCK_LANTERN_STEP)
                    sound = AVASounds.FOOTSTEP_METAL;
                else if (sound == SoundEvents.BLOCK_SAND_STEP || sound == SoundEvents.BLOCK_GRAVEL_STEP)
                    sound = AVASounds.FOOTSTEP_SAND;
                else if (sound == SoundEvents.BLOCK_STONE_STEP)
                    sound = AVASounds.FOOTSTEP_SOLID;
                else if (sound == SoundEvents.BLOCK_GRASS_STEP)
                    sound = AVASounds.FOOTSTEP_GRASS;
                else if (sound == SoundEvents.BLOCK_WET_GRASS_STEP)
                    sound = AVASounds.FOOTSTEP_FLOOD;
                else if (sound == SoundEvents.BLOCK_WOOL_STEP)
                    sound = AVASounds.FOOTSTEP_WOOL;
                else
                    sound = null;
                if (sound != null)
                {
                    player.playSound(sound, SoundCategory.PLAYERS, 1.0F, 1.0F);
                    if (player.getEntityWorld().isRainingAt(player.getPosition()))
                        player.playSound(AVASounds.FOOTSTEP_FLOOD, SoundCategory.PLAYERS, 1.0F, 1.0F);
                    capability.setStepSoundCooldown(player.isSprinting() ? 5 : 8);
                }
            }
        }
        float recoil = capability.getRecoil();
        float spread = capability.getSpread();
        float shake = capability.getShake();
        RecoilRefundTypeCommand.RefundType refundType = WorldData.getCap(player.world).getRecoilRefundType();
        if (!capability.isFiring())
        {
            float decr = 0.2F;
            boolean isGun = stack.getItem() instanceof AVAItemGun;
            boolean isLinear = refundType == RecoilRefundTypeCommand.RefundType.LINEAR;
            float amount;
            if (recoil != 0)
            {
                if (isLinear)
                    amount = Math.min(recoil, isGun ? getGun(player).recoilRefund : (decr + 0.225F));
                else
                    amount = Math.min(recoil, Math.max(0.1F, recoil / 19.0F + 0.075F));
                capability.setRecoil(recoil - amount);
                if (refundType != RecoilRefundTypeCommand.RefundType.NONE) player.rotationPitch += amount;
            }
            if (spread != 0)
            {
                amount = spread - Math.min(spread, isGun ? getGun(player).getStability(true) : decr);
                capability.setSpread(amount);
            }
            if (shake != 0)
            {
                if (isLinear)
                {
                    if (shake > 0)
                        amount = Math.min(shake, (isGun ? getGun(player).getAccuracy(true) : decr) + 2.5F);
                    else
                        amount = Math.max(shake, (isGun ? -getGun(player).getAccuracy(true) : -decr) - 2.5F);
                }
                else
                {
                    if (shake > 0)
                        amount = Math.min(shake, Math.max(0.075F, shake / 21.0F + 0.075F));
                    else
                        amount = Math.max(shake, -Math.max(0.075F, shake / 21.0F + 0.075F));
                }
                capability.setShake(shake - amount);
                if (refundType != RecoilRefundTypeCommand.RefundType.NONE) player.rotationYaw -= amount;
            }
        }
        int uav = capability.getUAVDuration();
        if (uav > 0)
            capability.setUAVDuration(player, uav - 1);
        int healthBoost = capability.getHealthBoost() * 3;
        if (!player.world.isRemote())
        {
            boolean shouldApply = false;
            if (hasHealthBoostModifier(player) && (Objects.requireNonNull(getHealthBoostModifier(player)).getAmount() != healthBoost))
            {
                removeHealthBoostModifier(player);
                shouldApply = true;
            }
            else if (!hasHealthBoostModifier(player) && healthBoost != 0)
                shouldApply = true;
            if (shouldApply)
                applyHealthBoostModifier(player, createHealthBoostModifier(healthBoost));
            if (stack.getItem() instanceof Binocular && stack.getOrCreateTag().getBoolean("aiming") && world.getGameTime() % AVAServerConfig.BIONCULAR_RANGE_UPDATE_INTERVAL.get() == 0L)
                world.addEntity(new BinocularRTBulletEntity(world, player, true));
        }
        CompoundNBT compound = player.getPersistentData();
        compound.putDouble("lastposx", player.getPosX());
        compound.putDouble("lastposy", player.getPosY());
        compound.putDouble("lastposz", player.getPosZ());
    }

    @SubscribeEvent
    public static void onPlayerDeath(LivingDeathEvent event)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () ->
        {
            if (isAVADamageSource(event.getSource()) && event.getEntityLiving() instanceof PlayerEntity)
                onPlayerDeath(event.getSource(), (PlayerEntity) event.getEntityLiving());
        });
    }

    @OnlyIn(Dist.CLIENT)
    private static void onPlayerDeath(DamageSource source, PlayerEntity player)
    {
        PlayerEntity local = Minecraft.getInstance().player;
        if (local != null && local.isAlive())
            cap(local).addKillTip(new KillTips.KillTip((source.getTrueSource() instanceof PlayerEntity ? (PlayerEntity) source.getTrueSource() : null), ((AVADamageSource.IWeapon) source).getWeapon(), player));
    }

    @SubscribeEvent
    public static void onPlayerHurt(LivingDamageEvent event)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () ->
        {
            if (event.getEntityLiving() instanceof PlayerEntity)
                onPlayerHurt(event.getSource(), (PlayerEntity) event.getEntityLiving());
        });
    }

    @OnlyIn(Dist.CLIENT)
    private static void onPlayerHurt(DamageSource source, PlayerEntity player)
    {
        PlayerEntity local = Minecraft.getInstance().player;
        if (isFullEquipped(player) && local != null && local.isAlive() && AVAClientEvent.isSamePlayer(player, local) && source.getTrueSource() != null)
            cap(local).addHurtInfo(source.getTrueSource());
    }

    private static void syncWorldCapWithClient(ServerPlayerEntity player)
    {
        AVAPackets.INSTANCE.sendTo(new SyncCapabilityDataMessage(WorldData.getCap(player.world)), player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
    }
}
