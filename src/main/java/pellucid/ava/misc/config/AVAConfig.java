package pellucid.ava.misc.config;

import net.minecraftforge.common.ForgeConfigSpec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AVAConfig
{
    protected static ForgeConfigSpec.Builder BUILDER;

    protected static void makeSpace()
    {
        makeSpace(2);
    }

    protected static void makeSpace(int lines, String... extra)
    {
        List<String> spaces = new ArrayList<>();
        for (int i=0;i<lines;i++)
            spaces.add(" ");
        spaces.addAll(Arrays.asList(extra));
        BUILDER.comment(spaces.toArray(new String[0]));
    }
}
