package pellucid.ava.misc;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;

public class KillTips
{
    private ArrayList<KillTip> tips = new ArrayList<>();

    public void add(KillTip tip)
    {
        this.tips.add(tip);
    }

    public ArrayList<KillTip> getTips()
    {
        return this.tips;
    }

    /*@Override
    public CompoundNBT serializeNBT()
    {
        int i = 0;
        CompoundNBT compound = new CompoundNBT();
        compound.putInt("size", tips.size());
        for (KillTip tip : tips)
        {
            if (tip.killer != null)
                compound.putInt("killer" + i, tip.killer.getEntityId());
            ResourceLocation loc = tip.weapon.getRegistryName();
            if (loc != null)
                compound.putString("weapon" + i, loc.toString());
            else
                tip.isAvailable = false;
            compound.putBoolean("available" + i, tip.isAvailable);
            compound.putInt("player" + i, tip.player.getEntityId());
            i++;
        }
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> read(nbt));
    }*/

    /*@OnlyIn(Dist.CLIENT)
    private void read(CompoundNBT nbt)
    {
        CompoundNBT compound = (CompoundNBT) nbt.get("killtips");
        if (compound == null)
            return;
        for (int i=0;i<compound.getInt("size");i++)
        {
            KillTip tip = new KillTip();
            tip.isAvailable = compound.getBoolean("available" + i);
            World world = Minecraft.getInstance().world;
            if (world != null)
            {
                Entity entity;
                if (compound.contains("killer" + i))
                {
                    entity = world.getEntityByID(compound.getInt("killer" + i));
                    if (entity instanceof PlayerEntity)
                        tip.killer = (PlayerEntity) entity;
                }
                tip.weapon = ForgeRegistries.ITEMS.getValue(new ResourceLocation(compound.getString("weapon")));
                entity = world.getEntityByID(compound.getInt("player" + i));
                if (entity instanceof PlayerEntity)
                    tip.killer = (PlayerEntity) entity;
                else
                    tip.isAvailable= false;
                this.tips.add(tip);
            }
            else
                tip.isAvailable = false;
        }
    }*/

    public static class KillTip
    {
        private PlayerEntity killer;
        private Item weapon;
        private PlayerEntity player;
        private boolean isAvailable;

        public KillTip() {}

        public KillTip(Item weapon, PlayerEntity player)
        {
            this(null, weapon, player);
        }

        public KillTip(@Nullable PlayerEntity killer, @Nonnull Item weapon, @Nonnull PlayerEntity player)
        {
            this.killer = killer;
            this.weapon = weapon;
            this.player = player;
        }

        public boolean hasKiller()
        {
            return this.killer != null;
        }

        public PlayerEntity getKiller()
        {
            return this.killer;
        }

        public Item getWeapon()
        {
            return this.weapon;
        }

        public PlayerEntity getPlayer()
        {
            return this.player;
        }

        public boolean isAvailable()
        {
            return this.isAvailable;
        }

        public void setAvailable(boolean value)
        {
            this.isAvailable = value;
        }
    }
}
