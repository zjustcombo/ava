package pellucid.ava.misc.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.misc.cap.WorldData;

public class SetGlassDestroyableCommand
{
    public static ArgumentBuilder<CommandSource, LiteralArgumentBuilder<CommandSource>> register(CommandDispatcher<CommandSource> dispatcher)
    {
        return Commands.literal("enableGlassDestroy")
                .requires(player -> player.hasPermissionLevel(3))
                .then(Commands.argument("value", BoolArgumentType.bool())
                .executes((context) ->
                {
                    boolean bool = BoolArgumentType.getBool(context, "value");
                    for (ServerWorld world : context.getSource().getServer().getWorlds())
                        WorldData.getCap(world).setGlassDestroyable(bool);
                    for (PlayerEntity player : context.getSource().getWorld().getPlayers())
                        player.sendMessage(new StringTextComponent("Server admin" + (bool ? " enabled" : " disabled") + " bullets to break glass"));
                    return bool ? 1 : 0;
                }));
    }
}
