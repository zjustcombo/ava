package pellucid.ava.misc.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.SyncCapabilityDataMessage;

public class RecoilRefundTypeCommand
{
    public static ArgumentBuilder<CommandSource, LiteralArgumentBuilder<CommandSource>> register(CommandDispatcher<CommandSource> dispatcher)
    {
        LiteralArgumentBuilder<CommandSource> builder = Commands.literal("setRecoilRefundType")
                .requires(player -> player.hasPermissionLevel(3));
        for (RefundType type : RefundType.values())
        {
            builder.then(Commands.literal(type.toString().toLowerCase()).executes((context) -> {
                for (World world : context.getSource().getServer().getWorlds())
                    WorldData.getCap(world).setRecoilRefundType(type);
                AVAPackets.INSTANCE.send(PacketDistributor.ALL.noArg(), new SyncCapabilityDataMessage(WorldData.getCap(context.getSource().getWorld())));
                for (PlayerEntity player : context.getSource().getWorld().getPlayers())
                    player.sendMessage(new StringTextComponent("Server admin has set all players' recoil type to " + type.toString().toLowerCase()));
                return type.ordinal();
            }));
        }
        return builder;
    }

    public enum RefundType
    {
        NONE,
        LINEAR,
        EXPONENTIAL,
    }
}
