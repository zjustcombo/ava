package pellucid.ava.misc.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;

public class AVACommands
{
    public AVACommands() {}

    public static void registerAll(CommandDispatcher<CommandSource> dispatcher)
    {
        LiteralArgumentBuilder<CommandSource> pt2command = Commands
                .literal("ava")
                .then(SetCrosshairServerCommand.register(dispatcher))
                .then(FriendlyFireCommand.register(dispatcher))
                .then(ReducedFriendlyFireCommand.register(dispatcher))
                .then(SetGlassDestroyableCommand.register(dispatcher))
                .then(SetMobDropsKitsCommand.register(dispatcher))
                .then(RecoilRefundTypeCommand.register(dispatcher))
                .then(BoostPlayerCommand.register(dispatcher));
        dispatcher.register(pt2command);
    }
}
