package pellucid.ava.misc.cap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import pellucid.ava.AVA;

@Mod.EventBusSubscriber(modid = AVA.MODID)
public class AVACapabilities
{
    public static final ResourceLocation PLAYER_ACTION_CAP = new ResourceLocation(AVA.MODID, "player_action");
    public static final ResourceLocation WORLD_DATA_CAP = new ResourceLocation(AVA.MODID, "world_data");

    public static void registerAll()
    {
        CapabilityManager.INSTANCE.register(IPlayerAction.class, new PlayerAction.PlayerActionStorage(), PlayerAction::new);
        CapabilityManager.INSTANCE.register(IWorldData.class, new WorldData.WorldDataStorage(), WorldData::new);
    }

    @SubscribeEvent
    public static void attachEntityCapability(final AttachCapabilitiesEvent<Entity> event)
    {
        if(event.getObject() instanceof PlayerEntity)
            if (!event.getCapabilities().containsKey(PLAYER_ACTION_CAP))
                event.addCapability(PLAYER_ACTION_CAP, new PlayerAction());
    }

    @SubscribeEvent
    public static void attachWorldCapability(final AttachCapabilitiesEvent<World> event)
    {
        if(event.getObject() != null)
            if (!event.getCapabilities().containsKey(WORLD_DATA_CAP))
                event.addCapability(WORLD_DATA_CAP, new WorldData());
    }
}
