package pellucid.ava.misc.cap;

import net.minecraftforge.common.capabilities.Capability;
import pellucid.ava.misc.commands.RecoilRefundTypeCommand;

public interface IWorldData
{
    void setShouldRenderCrosshair(boolean value);

    void setFriendlyFire(boolean value);

    void setReducedFriendlyFire(boolean value);

    void setGlassDestroyable(boolean value);

    void setMobDropsKitsChance(float chance);

    void setRecoilRefundType(RecoilRefundTypeCommand.RefundType type);

    boolean shouldRenderCrosshair();

    boolean isFriendlyFireAllowed();

    boolean isFriendlyFireReduced();

    boolean isGlassDestroyable();

    float getMobDropsKitsChance();

    RecoilRefundTypeCommand.RefundType getRecoilRefundType();

    Capability.IStorage<IWorldData> getStorage();
}
