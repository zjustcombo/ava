package pellucid.ava.misc.cap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.network.PacketDistributor;
import pellucid.ava.misc.HurtInfo;
import pellucid.ava.misc.KillTips;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.UAVMessage;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class PlayerAction implements ICapabilitySerializable<CompoundNBT>, IPlayerAction
{
    @CapabilityInject(IPlayerAction.class)
    public static Capability<IPlayerAction> PLAYER_ACTION_CAPABILITY = null;

    private final LazyOptional<IPlayerAction> lazyOptional = LazyOptional.of(PlayerAction::new);
    private boolean aiming;
    private float recoil;
    private float shake;
    private float spread;
    private boolean firing;
    private int flashDuration;
    private int stepSoundCooldown;
    private KillTips killTips = new KillTips();
    private HurtInfo hurtInfo = new HurtInfo();
    private int killTipCooldown;
    private int nightVisionBattery = 600;
    private boolean nightVisionActivated;
    private int uavMarkedDuration;
    private int attackDamageBoost = 0;
    private int healthBoost = 0;

    public PlayerAction() {}

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
    {
        return cap == PLAYER_ACTION_CAPABILITY ? lazyOptional.cast() : LazyOptional.empty();
    }

    public static IPlayerAction getCap(PlayerEntity player)
    {
        return player.getCapability(PLAYER_ACTION_CAPABILITY).orElseThrow(() -> new NullPointerException("getting capability"));
    }

    @Override
    public CompoundNBT serializeNBT()
    {
        return (CompoundNBT) PLAYER_ACTION_CAPABILITY.getStorage().writeNBT(PLAYER_ACTION_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during writing Player Capability")), null);
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt)
    {
        deserializeNBT(nbt, false);
    }

    public void deserializeNBT(CompoundNBT nbt, boolean fromServer)
    {
        ((PlayerActionStorage) PLAYER_ACTION_CAPABILITY.getStorage()).readNBT(PLAYER_ACTION_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during reading Player Capability")), null, nbt, fromServer);
    }

    @Override
    public void setAiming(boolean value)
    {
        this.aiming = value;
    }

    @Override
    public void setRecoil(float value)
    {
        this.recoil = value;
    }

    @Override
    public void setShake(float value)
    {
        this.shake = value;
    }

    @Override
    public void setSpread(float value)
    {
        this.spread = value;
    }

    @Override
    public void setFiring(boolean value)
    {
        this.firing = value;
    }

    @Override
    public void setFlashDuration(int value)
    {
        this.flashDuration = value;
    }

    @Override
    public void setStepSoundCooldown(int cooldown)
    {
     this.stepSoundCooldown = cooldown;
    }

    @Override
    public void addKillTip(KillTips.KillTip tip)
    {
        this.killTips.add(tip);
    }

    @Override
    public void setKillTips(KillTips tips)
    {
        this.killTips = tips;
    }

    @Override
    public void removeKillTip()
    {
        this.killTips.getTips().remove(0);
    }

    @Override
    public void setkillTipCooldown(int value)
    {
        this.killTipCooldown = value;
    }

    @Override
    public void addHurtInfo(Entity entity)
    {
        addHurtInfo(entity.getPositionVec());
    }

    @Override
    public void addHurtInfo(Vec3d pos)
    {
        hurtInfo.add(new HurtInfo.Info(pos));
    }

    @Override
    public void setNightVisionBattery(int value)
    {
        this.nightVisionBattery = value;
    }

    @Override
    public void setNightVisionActive(boolean value)
    {
        this.nightVisionActivated = value;
    }

    @Override
    public void setUAVDuration(@Nullable PlayerEntity player, int value)
    {
        this.uavMarkedDuration = value;
        if (player instanceof ServerPlayerEntity && (value == 0 || value == 140))
            AVAPackets.INSTANCE.send(PacketDistributor.TRACKING_CHUNK.with(() -> player.getEntityWorld().getChunkAt(player.getPosition())), new UAVMessage(player.getEntityId(), value));
    }

    @Override
    public void setAttackDamageBoost(int amount)
    {
        this.attackDamageBoost = Math.max(0, Math.min(20, amount));
    }

    @Override
    public void setHealthBoost(int amount)
    {
        this.healthBoost = Math.max(0, Math.min(20, amount));
    }

    @Override
    public boolean isAiming()
    {
        return this.aiming;
    }

    @Override
    public float getRecoil()
    {
        return this.recoil;
    }

    @Override
    public float getShake()
    {
        return this.shake;
    }

    @Override
    public float getSpread()
    {
        return this.spread;
    }

    @Override
    public boolean isFiring()
    {
        return this.firing;
    }

    @Override
    public int getFlashDuration()
    {
        return this.flashDuration;
    }

    @Override
    public int getStepSoundCooldown()
    {
        return this.stepSoundCooldown;
    }

    @Override
    public KillTips getKillTips()
    {
        return this.killTips;
    }

    @Override
    public HurtInfo getHurtInfo()
    {
        return hurtInfo;
    }

    @Override
    public int getkillTipCooldown()
    {
        return this.killTipCooldown;
    }

    @Override
    public int getNightVisionBattery()
    {
        return this.nightVisionBattery;
    }

    @Override
    public boolean isNightVisionActivated()
    {
        return this.nightVisionActivated;
    }

    @Override
    public int getUAVDuration()
    {
        return this.uavMarkedDuration;
    }

    @Override
    public int getAttackDamageBoost()
    {
        return this.attackDamageBoost;
    }

    @Override
    public int getHealthBoost()
    {
        return this.healthBoost;
    }

    public static class PlayerActionStorage implements Capability.IStorage<IPlayerAction>
    {
        public PlayerActionStorage(){}

        @Nullable
        @Override
        public INBT writeNBT(Capability<IPlayerAction> capability, IPlayerAction instance, Direction side)
        {
            CompoundNBT compound = new CompoundNBT();
            compound.putBoolean("aiming", instance.isAiming());
            compound.putFloat("recoil", instance.getRecoil());
            compound.putFloat("shake", instance.getShake());
            compound.putFloat("spread", instance.getSpread());
            compound.putBoolean("fire", instance.isFiring());
            compound.putInt("flash", instance.getFlashDuration());
            compound.putInt("step", instance.getStepSoundCooldown());
            compound.putInt("killtipcooldown", instance.getkillTipCooldown());
            compound.putInt("nvbattery", instance.getNightVisionBattery());
            compound.putBoolean("nvactive", instance.isNightVisionActivated());
            compound.putInt("uav", instance.getUAVDuration());
            compound.putInt("attackdamage", instance.getAttackDamageBoost());
            compound.putInt("health", instance.getHealthBoost());
            return compound;
        }

        @Override
        public void readNBT(Capability<IPlayerAction> capability, IPlayerAction instance, Direction side, INBT nbt)
        {
            readNBT(capability, instance, side, nbt, false);
        }

        public void readNBT(Capability<IPlayerAction> capability, IPlayerAction instance, Direction side, INBT nbt, boolean fromServer)
        {
            CompoundNBT compound = (CompoundNBT) nbt;
            instance.setAiming(compound.getBoolean("aiming"));
            instance.setRecoil(compound.getFloat("recoil"));
            instance.setShake(compound.getFloat("shake"));
            instance.setSpread(compound.getFloat("spread"));
            instance.setFiring(compound.getBoolean("fire"));
            instance.setAttackDamageBoost(compound.getInt("attackdamage"));
            instance.setHealthBoost(compound.getInt("health"));
            if (!fromServer)
            {
                instance.setFlashDuration(compound.getInt("flash"));
                instance.setStepSoundCooldown(compound.getInt("step"));
                instance.setNightVisionBattery(compound.getInt("nvbattery"));
                instance.setNightVisionActive(compound.getBoolean("nvactive"));
                instance.setUAVDuration(null, compound.getInt("uav"));
            }
        }
    }
}
