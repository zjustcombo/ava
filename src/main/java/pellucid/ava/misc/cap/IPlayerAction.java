package pellucid.ava.misc.cap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.Vec3d;
import pellucid.ava.misc.HurtInfo;
import pellucid.ava.misc.KillTips;

public interface IPlayerAction
{
    void setAiming(boolean value);

    void setRecoil(float value);

    void setShake(float value);

    void setSpread(float value);

    void setFiring(boolean value);

    void setFlashDuration(int value);

    void setStepSoundCooldown(int pos);

    void addKillTip(KillTips.KillTip tip);

    void setKillTips(KillTips tips);

    void removeKillTip();

    void setkillTipCooldown(int value);

    void addHurtInfo(Entity entity);

    void addHurtInfo(Vec3d pos);

    void setNightVisionBattery(int value);

    void setNightVisionActive(boolean value);

    void setUAVDuration(PlayerEntity player, int value);

    void setAttackDamageBoost(int amount);

    void setHealthBoost(int amount);

    boolean isAiming();

    float getRecoil();

    float getShake();

    float getSpread();

    boolean isFiring();

    int getFlashDuration();

    int getStepSoundCooldown();

    KillTips getKillTips();

    HurtInfo getHurtInfo();

    int getkillTipCooldown();

    int getNightVisionBattery();

    boolean isNightVisionActivated();

    int getUAVDuration();

    int getAttackDamageBoost();

    int getHealthBoost();
}
