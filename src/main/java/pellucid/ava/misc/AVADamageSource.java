package pellucid.ava.misc;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import pellucid.ava.items.init.Projectiles;

import javax.annotation.Nullable;

public class AVADamageSource
{
    public static AVAEntityDamageSource causeDamageDirect(Entity attacker, Item weapon)
    {
        return new AVAEntityDamageSource("direct", attacker, weapon);
    }

    public static AVAIndirectEntityDamageSource causeBulletDamageIndirect(Entity attacker, Entity bullet, Item weapon)
    {
        return new AVAIndirectEntityDamageSource("bullet", bullet, attacker, weapon);
    }

    public static AVAIndirectEntityDamageSource causeProjectileExplodeDamage(Entity attacker, Entity projectile, Item weapon)
    {
        return (AVAIndirectEntityDamageSource) new AVAIndirectEntityDamageSource("explosion", projectile, attacker, weapon).setExplosion();
    }

    public static AVAIndirectEntityDamageSource causeToxicGasDamage(Entity attacker, Entity projectile)
    {
        return (AVAIndirectEntityDamageSource) new AVAIndirectEntityDamageSource("toxic_gas", projectile, attacker, Projectiles.M18_TOXIC).setDamageBypassesArmor();
    }

    public static boolean isAVADamageSource(DamageSource source)
    {
        return source instanceof AVAEntityDamageSource || source instanceof AVAIndirectEntityDamageSource;
    }

    public static class AVAEntityDamageSource extends EntityDamageSource implements IWeapon
    {
        private final Item weapon;
        public AVAEntityDamageSource(String damageTypeIn, Entity damageSourceEntityIn, @Nullable Item weapon)
        {
            super("ava." + damageTypeIn, damageSourceEntityIn);
            this.weapon = weapon;
        }

        @Override
        public ITextComponent getDeathMessage(LivingEntity entity)
        {
            String s = "death.attack.";
            if (damageSourceEntity == null)
            {
                s += "ava.killed";
                if (this.weapon != null)
                    return new TranslationTextComponent(s + ".weapon", entity.getDisplayName(), this.weapon.getTranslationKey());
                return new TranslationTextComponent(s, entity.getDisplayName());
            }
            s += this.damageType;
            return this.weapon != null ? new TranslationTextComponent(s + ".weapon", entity.getDisplayName(), this.damageSourceEntity.getDisplayName(), new TranslationTextComponent(this.weapon.getTranslationKey())) : new TranslationTextComponent(s, entity.getDisplayName(), this.damageSourceEntity.getDisplayName());
        }

        public Item getWeapon()
        {
            return this.weapon;
        }
    }

    public static class AVAIndirectEntityDamageSource extends IndirectEntityDamageSource implements IWeapon
    {
        private final Item weapon;
        public AVAIndirectEntityDamageSource(String damageTypeIn, Entity source, @Nullable Entity indirectEntityIn, @Nullable Item weapon)
        {
            super("ava." + damageTypeIn, source, indirectEntityIn);
            this.weapon = weapon;
        }


        @Override
        public ITextComponent getDeathMessage(LivingEntity entity)
        {
            String s = "death.attack.";
            if (getTrueSource() == null)
            {
                s += "ava.killed";
                if (this.weapon != null)
                    return new TranslationTextComponent(s + ".weapon", entity.getDisplayName(), this.weapon.getTranslationKey());
                return new TranslationTextComponent(s, entity.getDisplayName());
            }
            s += this.damageType;
            return this.weapon != null ? new TranslationTextComponent(s + ".weapon", entity.getDisplayName(), getTrueSource().getDisplayName(), new TranslationTextComponent(this.weapon.getTranslationKey())) : new TranslationTextComponent(s, entity.getDisplayName(), getTrueSource().getDisplayName());
        }

        public Item getWeapon()
        {
            return this.weapon;
        }
    }

    public interface IWeapon
    {
        Item getWeapon();
    }
}
