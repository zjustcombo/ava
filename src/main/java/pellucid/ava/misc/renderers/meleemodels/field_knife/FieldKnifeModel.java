package pellucid.ava.misc.renderers.meleemodels.field_knife;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.meleemodels.ModifiedMeleeWeaponModel;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class FieldKnifeModel extends ModifiedMeleeWeaponModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0F, 17, 17),
            new Vector3f(-1.25F, 4F, 2.5F),
            new Vector3f(1F, 1F, 0.85F)
    );

    public static final Perspective RIGHT_HAND_ORIGINAL = new Perspective(new float[]{0, -15, 0}, new float[]{-0.1F, -0.375F, 0.125F}, ONE_A);

    protected static final ArrayList<Animation> LEFT_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(2, new Perspective(new float[]{9, 75, 76}, new float[]{-11F, 7.25F, 7.25F}, new float[]{1, 0.35F, 0.85F})));
        add(new Animation(4, new Perspective(new float[]{9, 17, 76}, new float[]{-10.5F, 7.25F, -0.75F}, new float[]{1, 1, 0.85F})));
        add(new Animation(6, new Perspective(new float[]{9, -65, 76}, new float[]{5F, 5.25F, 1.25F}, new float[]{1, 1, 0.85F})));
        add(new Animation(8, ORIGINAL_FP_RIGHT));
        add(new Animation(10, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective HIDDEN = new Perspective(new float[]{-18, 7, 46}, new float[]{0.175F, -0.175F, -0.275F}, new float[]{1.125F, 1.05F, 1.00F});
    public static final ArrayList<Animation> LEFT_HAND_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_ORIGINAL));
        add(new Animation(2, new Perspective(new float[]{-15F, 8F, -54F}, new float[]{-0.375F, -0.45F, -0.15F}, new float[]{0.45F, 1.05F, 0.625F})));
        add(new Animation(4, new Perspective(new float[]{-13, 8, 19}, new float[]{0.15F, -0.15F, -0.1F}, new float[]{1.125F, 1.05F, 1.00F})));
        add(new Animation(6, HIDDEN));
        add(new Animation(8, RIGHT_HAND_ORIGINAL));
        add(new Animation(10, RIGHT_HAND_ORIGINAL));
    }};

    protected static final ArrayList<Animation> RIGHT_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(7, new Perspective(new float[]{7, -17, 63}, new float[]{-0.5F, 3F, 5.25F}, new float[]{1, 1, 0.85F})));
        add(new Animation(9, new Perspective(new float[]{23, -26, 80}, new float[]{-2.5F, 5F, -6F}, new float[]{1, 1, 0.85F})));
        add(new Animation(10, new Perspective(new float[]{23, -26, 80}, new float[]{-2.5F, 5F, -6F}, new float[]{1, 1, 0.85F})));
        add(new Animation(13, ORIGINAL_FP_RIGHT));
        add(new Animation(20, ORIGINAL_FP_RIGHT));
    }};

    public static final ArrayList<Animation> RIGHT_HAND_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_ORIGINAL));
        add(new Animation(7, new Perspective(new float[]{0F, 0F, 17F}, new float[]{-0.4F, -0.2F, 0.2F}, ONE_A)));
        add(new Animation(9, new Perspective(new float[]{0F, 0F, 10F}, new float[]{-0.1F, -0.075F, -0.125F}, new float[]{0.675F, 1.3F, 0.85F})));
        add(new Animation(10, new Perspective(new float[]{0F, 0F, 10F}, new float[]{-0.1F, -0.075F, -0.125F}, new float[]{0.675F, 1.3F, 0.85F})));
        add(new Animation(14, RIGHT_HAND_ORIGINAL));
        add(new Animation(20, RIGHT_HAND_ORIGINAL));
    }};

    public FieldKnifeModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    protected ArrayList<Animation> getLeftAnimation()
    {
        return LEFT_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getRightAnimation()
    {
        return RIGHT_ANIMATION;
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                translation = new Vector3f(0, 1F, 2.5F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GUI:
                rotation = new Vector3f(79, 47, -93);
                translation = new Vector3f(-0.75F, -0.75F, 0);
                break;
            case FIXED:
                rotation = new Vector3f(93, 47, -93);
                translation = new Vector3f(-0.75F, -0.75F, -0.5F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }
}
