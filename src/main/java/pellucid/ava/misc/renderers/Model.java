package pellucid.ava.misc.renderers;

import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.client.model.data.EmptyModelData;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public class Model implements IBakedModel
{
    protected IBakedModel origin;
    protected final ModelGetter modifiedModel;

    public Model(IBakedModel origin, ModelGetter modifiedModel)
    {
        this.origin = origin;
        this.modifiedModel = modifiedModel;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        return this.origin.getQuads(state, side, rand);
    }

    @Override
    public boolean isAmbientOcclusion()
    {
        return this.origin.isAmbientOcclusion();
    }

    @Override
    public boolean isGui3d()
    {
        return this.origin.isGui3d();
    }

    @Override
    public boolean func_230044_c_()
    {
        return this.origin.func_230044_c_();
    }

    @Override
    public boolean isBuiltInRenderer()
    {
        return this.origin.isBuiltInRenderer();
    }

    @Override
    public TextureAtlasSprite getParticleTexture()
    {
        return this.origin.getParticleTexture(EmptyModelData.INSTANCE);
    }

    @Override
    public ItemOverrideList getOverrides()
    {
        return new Overrides(modifiedModel);
    }

    public static class Overrides extends ItemOverrideList
    {
        protected final ModelGetter modifiedModel;
        public Overrides(ModelGetter modifiedModel)
        {
            super();
            this.modifiedModel = modifiedModel;
        }

        @Nullable
        @Override
        public IBakedModel getModelWithOverrides(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
        {
            return modifiedModel.getModel(origin, stack, world, entity);
        }
    }

    public interface ModelGetter
    {
        BakedModel getModel(IBakedModel origin, ItemStack stack, World world, LivingEntity entity);
    }
}
