package pellucid.ava.misc.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.events.AVAClientEvent;

@OnlyIn(Dist.CLIENT)
public class SmokeRenderer extends AVAClientEvent
{
    public static final ResourceLocation SMOKE = new ResourceLocation(AVA.MODID + ":textures/entities/smoke.png");

    public static void render(SmokeGrenadeEntity entityIn, MatrixStack stack)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player == null || !entityIn.isLanded())
            return;
        Minecraft.getInstance().getTextureManager().bindTexture(SMOKE);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        drawTransparent(true);
        RenderSystem.enableDepthTest();
        RenderSystem.enableAlphaTest();
        stack.rotate(minecraft.getRenderManager().getCameraOrientation());
        float f = entityIn.fadeAnimation / 8.0F;
        int[] rgb = entityIn.rgb;
        RenderSystem.color4f(rgb[0] / 255.0F, rgb[1] / 255.0F, rgb[2] / 255.0F, entityIn.fadeAnimation / 200F - 0.15F);
        stack.push();
        stack.scale(f, f, f);
        draw(bufferbuilder, tessellator, stack);
        stack.pop();
        f = entityIn.fadeAnimation / 11.0F;
        for (int x=-3;x<4;x+=2)
        {
            stack.push();
            stack.translate(x / 2.0F, 0.0F, 0.0F);
            stack.scale(f, f, f);
            draw(bufferbuilder, tessellator, stack);
            stack.pop();
        }
        for (int y=-1;y<2;y+=2)
        {
            stack.push();
            stack.translate(0.0F, y / 2.0F, 0.0F);
            stack.scale(f, f, f);
            draw(bufferbuilder, tessellator, stack);
            stack.pop();
        }
        for (int z=-3;z<4;z+=2)
        {
            stack.push();
            stack.translate(0.0F, 0.0F, z / 2.0F);
            stack.scale(f, f, f);
            draw(bufferbuilder, tessellator, stack);
            stack.pop();
        }
        drawTransparent(false);
        RenderSystem.disableDepthTest();
        RenderSystem.disableAlphaTest();
    }

    private static void draw(BufferBuilder bufferbuilder, Tessellator tessellator, MatrixStack stack)
    {
        Matrix4f stack4f = stack.getLast().getMatrix();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(stack4f, -0.25F, +0.25F, 0.0F).tex(0.0F, 1.0F).endVertex();
        bufferbuilder.pos(stack4f, +0.25F, +0.25F, 0.0F).tex(1.0F, 1.0F).endVertex();
        bufferbuilder.pos(stack4f, +0.25F, -0.25F, 0.0F).tex(1.0F, 0.0F).endVertex();
        bufferbuilder.pos(stack4f, -0.25F, -0.25F, 0.0F).tex(0.0F, 0.0F).endVertex();
        tessellator.draw();
    }
}
