package pellucid.ava.misc.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.IHasArm;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import pellucid.ava.AVA;
import pellucid.ava.events.AVAClientEvent;
import pellucid.ava.items.guns.AVAItemGun;

import java.lang.reflect.Field;
import java.util.List;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class AVATPRenderer extends AVAClientEvent
{

    private static final Field FIELD_177097_H = ObfuscationReflectionHelper.findField(LivingRenderer.class, "field_177097_h");
    public static final ResourceLocation UAV = new ResourceLocation(AVA.MODID + ":textures/overlay/uav.png");
    @SubscribeEvent
    public static void renderPlayerEvent(RenderPlayerEvent.Pre event)
    {
        AbstractClientPlayerEntity player = (AbstractClientPlayerEntity) event.getPlayer();
        if (getHeldStack(player).getItem() instanceof AVAItemGun)
        {
            if (!shouldRender(player))
                return;
            PlayerModel<AbstractClientPlayerEntity> model = event.getRenderer().getEntityModel();
            PlayerRenderer renderer = event.getRenderer();
            try
            {
                FIELD_177097_H.setAccessible(true);
//                List<LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>>> layerRenderers = ObfuscationReflectionHelper.getPrivateValue(LivingRenderer.class, renderer, "field_177097_h");
                List<LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>>> layerRenderers = (List<LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>>>) FIELD_177097_H.get(renderer);
                if (layerRenderers != null)
                {
                    if (layerRenderers.get(0).getClass() == BipedArmorLayer.class)
                        layerRenderers.set(0, new AVAArmourLayer(renderer, new BipedModel(0.5F), new BipedModel(1.0F)));
                    if (layerRenderers.get(1).getClass() == HeldItemLayer.class)
                        layerRenderers.set(1, new AVAHeldItemLayer(renderer));
                }
            }
            catch(Exception e)
            {
                AVA.LOGGER.fatal("Can not access protected field!");
            }
            model.bipedLeftArm.showModel = false;
            model.bipedLeftArmwear.showModel = false;
            model.bipedRightArm.showModel = false;
            model.bipedRightArmwear.showModel = false;
        }
    }

    @SubscribeEvent
    public static void renderPlayerEvent(RenderPlayerEvent.Post event)
    {
        AbstractClientPlayerEntity player = (AbstractClientPlayerEntity) event.getPlayer();
        if (getHeldStack(event.getPlayer()).getItem() instanceof AVAItemGun)
        {
            AVAItemGun gun = getGun(player);
            int type = player.isSprinting() ? gun.getRunAnimation() : gun.getHeldAnimation();
            if (!shouldRender(player))
                return;
            PlayerRenderer renderer = event.getRenderer();
            PlayerModel<AbstractClientPlayerEntity> model = renderer.getEntityModel();
            ModelRenderer leftArm = model.bipedLeftArm;
            ModelRenderer leftArmwear = model.bipedLeftArmwear;
            ModelRenderer rightArm = model.bipedRightArm;
            ModelRenderer rightArmwear = model.bipedRightArmwear;
            int p = gun.initTags(getHeldStack(player)).getInt("run");
            boolean phase = p <= 3 || (p > 6 && p <= 9);
            if (player.isSprinting())
                switch (type)
                {
                    case 1:
                    default:
                        leftArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset - 15.0F)) * -5.0F;
                        leftArm.rotationPointY = 20;
                        leftArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset - 15.0F)) * -5.0F;
                        leftArm.rotateAngleX = phase ? -90.5F : -90.75F;
                        //                        leftArm.rotateAngleX = -90.75F;
                        leftArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) + 66.0F;

                        rightArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset + 35.0F)) * 7.0F;
                        rightArm.rotationPointY = 20;
                        rightArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset + 35.0F)) * 7.0F;
                        rightArm.rotateAngleX = phase ? -89.75F : -90.0F;
                        //                        rightArm.rotateAngleX =-90.0F;
                        rightArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) + 65.75F;
                        break;
                    case 2:
                        leftArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset)) * -4.25F;
                        leftArm.rotationPointY = 20;
                        leftArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset)) * -4.25F;
                        leftArm.rotateAngleX = -90.0F;
                        leftArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) - 10.025F;

                        rightArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset)) * 4.25F;
                        rightArm.rotationPointY = 20;
                        rightArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset)) * 4.25F;
                        rightArm.rotateAngleX = -90.0F;
                        rightArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) + 3.125F;
                        break;
                }
            else
                switch (type)
                {
                    case 1:
                    default:
                        leftArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset + 35.0F)) * -4.75F;
                        leftArm.rotationPointY = player.isSneaking() ? 17 : 20;
                        leftArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset + 35.0F)) * -4.75F;
                        leftArm.rotateAngleX = -89.75F;
                        leftArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) - 9.675F;

                        rightArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset + 25.0F)) * 4.25F;
                        rightArm.rotationPointY = player.isSneaking() ? 17 : 20;
                        rightArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset + 25.0F)) * 4.25F;
                        rightArm.rotateAngleX = -90.0F;
                        rightArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) + 3.225F;
                        break;
                    case 2:
                        leftArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset)) * -4.25F;
                        leftArm.rotationPointY = player.isSneaking() ? 17 : 20;
                        leftArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset)) * -4.25F;
                        leftArm.rotateAngleX = -90.0F;
                        leftArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) - 10.025F;

                        rightArm.rotationPointX = -MathHelper.cos((float) Math.toRadians(player.renderYawOffset)) * 4.25F;
                        rightArm.rotationPointY = player.isSneaking() ? 17 : 20;
                        rightArm.rotationPointZ = -MathHelper.sin((float) Math.toRadians(player.renderYawOffset)) * 4.25F;
                        rightArm.rotateAngleX = -90.0F;
                        rightArm.rotateAngleY = (float) -Math.toRadians(player.renderYawOffset) + 3.125F;
                        break;
                }

            leftArmwear.copyModelAngles(leftArm);
            rightArmwear.copyModelAngles(leftArm);

            MatrixStack stack = event.getMatrixStack();
            IVertexBuilder buffer = event.getBuffers().getBuffer(model.getRenderType(player.getLocationSkin()));
            int light = event.getLight();
            int texture = OverlayTexture.NO_OVERLAY;
            leftArm.showModel = true;
            leftArm.render(stack, buffer, light, texture);
            leftArmwear.showModel = true;
            leftArmwear.render(stack, buffer, light, texture);
            rightArm.showModel = true;
            rightArm.render(stack, buffer, light, texture);
            rightArmwear.showModel = true;
            rightArmwear.render(stack, buffer, light, texture);
        }
    }

    @SubscribeEvent
    public static void renderUAV(RenderPlayerEvent.Post event)
    {
        PlayerEntity local = Minecraft.getInstance().player;
        PlayerEntity player = event.getPlayer();
        if (local == null || !player.isAlive() || cap(player).getUAVDuration() <= 0 || !(player instanceof AbstractClientPlayerEntity))
            return;
        MatrixStack stack = event.getMatrixStack();
        Minecraft.getInstance().getTextureManager().bindTexture(UAV);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        drawTransparent(true);
        stack.push();
        stack.translate(0.0D, (double)player.getHeight() / 2.0F, 0.0D);
        stack.rotate(event.getRenderer().getRenderManager().getCameraOrientation());
        float f = player.getWidth() * 5.0F;
        stack.scale(f, f, f);
        Matrix4f stack4f = stack.getLast().getMatrix();
        boolean self = self(player, local);
        RenderSystem.color4f(isOnSameTeam(player, local, false) ? 0.0F : self ? 255.0F : 200.0F, self ? 200.0F : 0.0F, isOnSameTeam(player, local, false) ? 255.0F : 0.0F, 0.45F);
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(stack4f, - 0.25F, + 0.215F, 0.0F).tex(0.0F, 1.0F).endVertex();
        bufferbuilder.pos(stack4f, + 0.25F, + 0.215F, 0.0F).tex(1.0F, 1.0F).endVertex();
        bufferbuilder.pos(stack4f, + 0.25F, - 0.215F, 0.0F).tex(1.0F, 0.0F).endVertex();
        bufferbuilder.pos(stack4f, - 0.25F, - 0.215F, 0.0F).tex(0.0F, 0.0F).endVertex();
        tessellator.draw();
        stack.pop();
        drawTransparent(false);
    }

    private static boolean shouldRender(PlayerEntity player)
    {
        return player.isAlive() && !player.isSwimming() && player.getHeldItemOffhand().isEmpty() && !player.isSpectator() && !player.isElytraFlying();
    }

    public static class AVAArmourLayer extends BipedArmorLayer
    {
        private final BipedModel modelLeggings;
        private final BipedModel modelArmor;
        public AVAArmourLayer(IEntityRenderer renderer, BipedModel p_i50936_2_, BipedModel p_i50936_3_)
        {
            super(renderer, p_i50936_2_, p_i50936_3_);
            this.modelLeggings = p_i50936_2_;
            this.modelArmor = p_i50936_3_;
        }

        @Override
        public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, Entity player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
        {
            this.renderArmorPart(matrixStackIn, bufferIn, (AbstractClientPlayerEntity) player, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, EquipmentSlotType.HEAD, packedLightIn);
            this.renderArmorPart(matrixStackIn, bufferIn, (AbstractClientPlayerEntity) player, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, EquipmentSlotType.CHEST, packedLightIn);
            this.renderArmorPart(matrixStackIn, bufferIn, (AbstractClientPlayerEntity) player, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, EquipmentSlotType.LEGS, packedLightIn);
            this.renderArmorPart(matrixStackIn, bufferIn, (AbstractClientPlayerEntity) player, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, EquipmentSlotType.FEET, packedLightIn);
        }

        private void renderArmorPart(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, AbstractClientPlayerEntity player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, EquipmentSlotType slotIn, int packedLightIn)
        {
            ItemStack itemstack = player.getItemStackFromSlot(slotIn);
            if (itemstack.getItem() instanceof ArmorItem) {
                ArmorItem armoritem = (ArmorItem)itemstack.getItem();
                if (armoritem.getEquipmentSlot() == slotIn) {
                    BipedModel model = (slotIn == EquipmentSlotType.LEGS) ? this.modelLeggings : this.modelArmor;
                    model = getArmorModelHook(player, itemstack, slotIn, model);
                    ((BipedModel)this.getEntityModel()).setModelAttributes(model);
                    model.setLivingAnimations(player, limbSwing, limbSwingAmount, partialTicks);
                    this.setModelSlotVisible(model, slotIn);
                    model.setRotationAngles(player, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
                    ModelRenderer leftArm = model.bipedLeftArm;
                    ModelRenderer rightArm = model.bipedRightArm;
                    if (getHeldStack(player).getItem() instanceof AVAItemGun && !player.isElytraFlying())
                    {
                        AVAItemGun gun = getGun(player);
                        if (!player.isSprinting())
                        {
                            switch (gun.getHeldAnimation())
                            {
                                case 1:
                                    leftArm.rotationPointX -= 2.15F;
                                    leftArm.rotationPointY -= 0.45F;
                                    leftArm.rotationPointZ += 0.75F;
                                    leftArm.rotateAngleY = 0.215F;
                                    leftArm.rotateAngleZ = 0.05F;

                                    rightArm.rotationPointX += 3.00F;
                                    rightArm.rotationPointY += 1.30F;
                                    rightArm.rotationPointZ += 2.00F - 0.08617267F;
                                    rightArm.rotateAngleX = -1.15F;
                                    rightArm.rotateAngleY = -0.125F;
                                    if ((player.getPosX() != player.lastTickPosX || player.getPosZ() != player.lastTickPosZ))
                                        leftArm.rotateAngleX = 0.013358736F + 199.75F;
                                    else
                                        leftArm.rotateAngleX = 199.75F;
                                default:
                                    break;
                                case 2:
                                    leftArm.rotationPointX -= 1.75F;
                                    leftArm.rotationPointY += 0.75F;
                                    leftArm.rotationPointZ += 0.75F;
                                    leftArm.rotateAngleY = 0.7F;

                                    rightArm.rotationPointX += 2.90F;
                                    rightArm.rotationPointY -= 0.25F;
                                    rightArm.rotationPointZ -= 0.85F;
                                    rightArm.rotateAngleX = -0.95F;
                                    rightArm.rotateAngleY = 0.15F;
                                    if ((player.getPosX() != player.lastTickPosX || player.getPosZ() != player.lastTickPosZ))
                                    {
                                        leftArm.rotateAngleX = 0.013358736F + 200.0F;
                                        leftArm.rotateAngleZ = -0.08617267F;
                                    }
                                    else
                                        leftArm.rotateAngleX = 200.0F;
                                    break;
                            }
                        }
                        else
                        {
                            int p = gun.initTags(getHeldStack(player)).getInt("run");
                            boolean phase = p <= 3 || (p > 6 && p <= 9);
                            switch (getGun(player).getRunAnimation())
                            {
                                case 1:
                                    leftArm.rotationPointX -= 1.7F;
                                    leftArm.rotationPointY += phase ? 0.85 : 0.25F;
                                    leftArm.rotationPointZ += 0.85F;
                                    leftArm.rotateAngleY = phase ? 0 : -0.3F;
                                    leftArm.rotateAngleZ = phase ? -0.2F : 0.0F;

                                    rightArm.rotationPointX += 0.45F;
                                    rightArm.rotationPointY += 0.60F;
                                    rightArm.rotationPointZ += 4.55F - 0.08617267F;
                                    rightArm.rotateAngleX = phase ? -1.35F : -1.15F;
                                    rightArm.rotateAngleY = 0.175F;
                                    if ((player.getPosX() != player.lastTickPosX || player.getPosZ() != player.lastTickPosZ))
                                        leftArm.rotateAngleX = 0.013358736F + (phase ? 200.45F : 200.7F);
                                    else
                                        leftArm.rotateAngleX = phase ? 200.45F : 200.7F;
                                default:
                                    break;
                                case 2:
                                    leftArm.rotationPointX -= 1.75F;
                                    leftArm.rotationPointY += 0.75F;
                                    leftArm.rotationPointZ += 0.75F;
                                    leftArm.rotateAngleY = 0.7F;

                                    rightArm.rotationPointX += 2.90F;
                                    rightArm.rotationPointY -= 0.25F;
                                    rightArm.rotationPointZ -= 0.85F;
                                    rightArm.rotateAngleX = -0.95F;
                                    rightArm.rotateAngleY = 0.15F;
                                    if ((player.getPosX() != player.lastTickPosX || player.getPosZ() != player.lastTickPosZ))
                                    {
                                        leftArm.rotateAngleX = 0.013358736F + 200.0F;
                                        leftArm.rotateAngleZ = -0.08617267F;
                                    }
                                    else
                                        leftArm.rotateAngleX = 200.0F;
                                    break;
                            }
                        }
                    }
                    boolean flag1 = itemstack.hasEffect();
                    if (armoritem instanceof net.minecraft.item.IDyeableArmorItem) { // Allow this for anything, not only cloth
                        int i = ((net.minecraft.item.IDyeableArmorItem)armoritem).getColor(itemstack);
                        float f = (float)(i >> 16 & 255) / 255.0F;
                        float f1 = (float)(i >> 8 & 255) / 255.0F;
                        float f2 = (float)(i & 255) / 255.0F;
                        renderArmor(matrixStackIn, bufferIn, packedLightIn, flag1, model, f, f1, f2, this.getArmorResource(player, itemstack, slotIn, null));
                        renderArmor(matrixStackIn, bufferIn, packedLightIn, flag1, model, 1.0F, 1.0F, 1.0F, this.getArmorResource(player, itemstack, slotIn, "overlay"));
                    } else {
                        renderArmor(matrixStackIn, bufferIn, packedLightIn, flag1, model, 1.0F, 1.0F, 1.0F, this.getArmorResource(player, itemstack, slotIn, null));
                    }
                }
            }
        }

        private void renderArmor(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, boolean glintIn, BipedModel modelIn, float red, float green, float blue, ResourceLocation armorResource) {
            IVertexBuilder ivertexbuilder = ItemRenderer.getBuffer(bufferIn, RenderType.getEntityCutoutNoCull(armorResource), false, glintIn);
            modelIn.render(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, red, green, blue, 1.0F);
        }
    }

    public static class AVAHeldItemLayer extends HeldItemLayer
    {
        public AVAHeldItemLayer(IEntityRenderer renderer)
        {
            super(renderer);
        }
        @Override
        public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, Entity entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
            AbstractClientPlayerEntity player = (AbstractClientPlayerEntity) entity;
            boolean flag = player.getPrimaryHand() == HandSide.RIGHT;
            ItemStack itemstack = flag ? player.getHeldItemOffhand() : player.getHeldItemMainhand();
            ItemStack itemstack1 = flag ? player.getHeldItemMainhand() : player.getHeldItemOffhand();
            if (!itemstack.isEmpty() || !itemstack1.isEmpty()) {
                matrixStackIn.push();
                if (this.getEntityModel().isChild) {
                    float f = 0.5F;
                    matrixStackIn.translate(0.0D, 0.75D, 0.0D);
                    matrixStackIn.scale(f, f, f);
                }

                this.doRender(player, itemstack1, ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND, HandSide.RIGHT, matrixStackIn, bufferIn, packedLightIn);
                this.doRender(player, itemstack, ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND, HandSide.LEFT, matrixStackIn, bufferIn, packedLightIn);
                matrixStackIn.pop();
            }
        }

        private void doRender(LivingEntity entityIn, ItemStack stackIn, ItemCameraTransforms.TransformType transformTypeIn, HandSide side, MatrixStack matrixIn, IRenderTypeBuffer bufferIn, int combinedLightIn) {
            if (!stackIn.isEmpty()) {
                matrixIn.push();
                if (!(entityIn instanceof PlayerEntity) || !(getHeldStack((PlayerEntity) entityIn).getItem() instanceof AVAItemGun))
                    ((IHasArm)this.getEntityModel()).translateHand(side, matrixIn);
                matrixIn.rotate(Vector3f.XP.rotationDegrees(-110.0F));
                matrixIn.rotate(Vector3f.YP.rotationDegrees(180.0F));
                boolean flag = side == HandSide.LEFT;
                matrixIn.translate((float)(flag ? -1 : 1) / 16.0F, 0.125D, -0.625D);
                if (entityIn instanceof PlayerEntity && (getHeldStack((PlayerEntity) entityIn).getItem() instanceof AVAItemGun) && !entityIn.isElytraFlying())
                {
                    PlayerEntity player = (PlayerEntity) entityIn;
                    AVAItemGun gun = getGun(player);
                    if (!player.isSprinting())
                        if (!player.isSneaking())
                            matrixIn.translate(0.115F, 0.25F, 0.01F);
                        else
                            matrixIn.translate(0.125F, 0.05F, -0.2F);
                    else
                    {
                        int p = gun.initTags(getHeldStack(player)).getInt("run");
                        boolean phase = p <= 3 || (p > 6 && p <= 9);
                        switch (gun.getRunAnimation())
                        {
                            case 1:
                            default:
                                matrixIn.translate(0.2F, 0.03F, phase ? -0.05F : -0.2F);
                                matrixIn.rotate(Vector3f.XP.rotationDegrees(-25F));
                                matrixIn.rotate(Vector3f.YP.rotationDegrees(-20F));
                                matrixIn.rotate(Vector3f.ZP.rotationDegrees(75F));
                                break;
                            case 2:
                                matrixIn.translate(0.075F, 0.35F, 0.25F);
                                matrixIn.rotate(Vector3f.XP.rotationDegrees(65F));
                                break;
                        }
                    }
                }
                Minecraft.getInstance().getFirstPersonRenderer().renderItemSide(entityIn, stackIn, transformTypeIn, flag, matrixIn, bufferIn, combinedLightIn);
                matrixIn.pop();
            }
        }
    }
}
