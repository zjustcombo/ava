package pellucid.ava.misc.renderers.models.gm94;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GM94Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, -3, 0),
            new Vector3f(-3.75F, 2.5F, -2F),
            new Vector3f(1, 1, 0.5F)
    );

    private static final float[] RUN_ROTATION = new float[]{0, 76, 14};
    private static final float[] RUN_SCALE = new float[]{0.5F, 0.75F, 0.75F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-10.25F, 3.75F, 5.75F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-10.75F, 3.25F, 5.75F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-11.25F, 3.75F, 5.75F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    protected static final float[] RELOAD_ROTATION = new float[]{0, -34F, 40F};
    protected static final float[] RELOAD_SCALE = new float[]{1, 1, 0.5F};
    private static final Perspective RELOAD_1 = new Perspective(RELOAD_ROTATION, new float[]{-1.5F, 2.5F, 2.25F}, RELOAD_SCALE);
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(4, RELOAD_1));
        add(new Animation(26, RELOAD_1));
        add(new Animation(30, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 35.0F}, new float[]{0.2F, -1.0F, -0.2F}, new float[]{0.45F, 1, 0.45F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 35.0F}, new float[]{0.15F, -0.65F, -0.25F}, new float[]{0.45F, 1, 0.45F});
    private static final Perspective LEFT_HAND_RELOADING_2_FP = new Perspective(new float[]{0, 0, 35.0F}, new float[]{0.05F, -0.65F, -0.2F}, new float[]{0.45F, 1, 0.45F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(4, LEFT_HAND_RELOADING_FP));
        add(new Animation(12, LEFT_HAND_RELOADING_2_FP));
        add(new Animation(14, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(16, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(18, LEFT_HAND_RELOADING_FP));
        add(new Animation(26, LEFT_HAND_RELOADING_2_FP));
        add(new Animation(30, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9.036F, 2.5F, 5.25F}, new float[]{1, 1, 0.5F});

    public static final ModelResourceLocation LID_CLOSE = new ModelResourceLocation("ava:gm94/gm94_lid_close#inventory");
    public static final ModelResourceLocation LID_OPEN = new ModelResourceLocation("ava:gm94/gm94_lid_open#inventory");
    public static final ModelResourceLocation GRENADE = new ModelResourceLocation("ava:gm94/gm94_grenade#inventory");

    public GM94Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        if (reload != 0)
            quads.addAll(this.getGrenadeQuads());
        quads.addAll(this.getLidQuads(state, rand));
        return quads;
    }

    protected ModelResourceLocation getCloseLid()
    {
        return LID_CLOSE;
    }

    protected ModelResourceLocation getOpenLid()
    {
        return LID_OPEN;
    }

    protected ModelResourceLocation getGrenade()
    {
        return GRENADE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getLidQuads(@Nullable BlockState state, Random rand)
    {
        //todo: use rotation instead of two different models
        return get(reload == 0 ? getCloseLid() : getOpenLid(), state, rand);
    }

    protected List<BakedQuad> getGrenadeQuads()
    {
        return get(getGrenade(), (newQuad) -> {
            if (translateQuadFrom(newQuad, 2.5F, reload, 4, 12, Direction.DOWN))
                translateQuadFrom(newQuad, 7.0F, reload, 4, 12, Direction.NORTH);
            else if (translateQuadFrom(newQuad, 2.5F, reload, 18, 26, Direction.DOWN))
                translateQuadFrom(newQuad, 7.0F, reload, 18, 26, Direction.NORTH);
        }, (reload >= 12 && reload < 18));
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(LID_CLOSE);
        ModelLoader.addSpecialModel(LID_OPEN);
        ModelLoader.addSpecialModel(GRENADE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-1.75F, 12.75F, 2.25F);
                scale = v3f(1.0F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, 0F, -7F);
                scale = v3f(0.8F);
                break;
            case GUI:
                rotation = new Vector3f(-79F, 46F, 57);
                translation = new Vector3f(-2.25F, -2.5F, 0);
                scale = v3f(0.5F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(6.25F, 0F, -1F);
                scale = v3f(0.8F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
