package pellucid.ava.misc.renderers.models.sr_25;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class Sr25KnutModel extends Sr25Model
{

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:sr_25/sr_25_knut_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:sr_25/sr_25_knut_slide#inventory");

    public Sr25KnutModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    @Override
    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }
}
