package pellucid.ava.misc.renderers.models.xm8;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Xm8Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(1, 0, 0),
            new Vector3f(-2.5F, 2.5F, 0.5F),
            new Vector3f(1, 1, 0.5F)
    );

    private static final float[] RUN_ROTATION = new float[]{-84, 85, 98};
    private static final float[] RUN_SCALE = new float[]{0.85F, 0.85F, 0.85F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-5.75F, 0.75F, 2.25F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-7, 0, 2.25F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-8.25F, 0.5F, 2.25F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    protected static final float[] RELOAD_TRANSLATION = new float[]{-1.25F, 1, -0.75F};
    protected static final float[] RELOAD_SCALE = new float[]{1.0F, 1.0F, 1.0F};
    private static final Perspective RELOAD_1 = new Perspective(new float[]{-23, 30, -24}, new float[]{-1.25F, 2.5F, -2}, RELOAD_SCALE);
    private static final Perspective RELOAD_2 = new Perspective(new float[]{-26, 2, -10}, RELOAD_TRANSLATION, RELOAD_SCALE);
    private static final Perspective RELOAD_3 = new Perspective(new float[]{-26, -6, -10}, RELOAD_TRANSLATION, RELOAD_SCALE);
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(4, RELOAD_1));
        add(new Animation(20, RELOAD_1));
        add(new Animation(26, RELOAD_2));
        add(new Animation(36, RELOAD_3));
        add(new Animation(40, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 35}, new float[]{-0.05F, -0.7F, -0.3F}, new float[]{0.5F, 0.5F, 1.0F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 35}, new float[]{-0.35F, 0.025F, -0.0125F}, new float[]{0.5F, 0.5F, 1.0F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(22, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(29, LEFT_HAND_RELOADING_FP));
        add(new Animation(40, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9.005F, 4.38F, 9.75F}, new float[]{1F, 1F, 0.1F});

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:xm8/xm8_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:xm8/xm8_slide#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:xm8/xm8_fire#inventory");

    public Xm8Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getMagazineQuads());
        quads.addAll(getSlideQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getSlideQuads()
    {
        return get(getSlide(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 3.5F, reload, 25, 34, Direction.SOUTH))
                translateQuadFrom(newQuad, 3.0F, reload, 34, 36, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 6.0F, reload, 5, 10, Direction.DOWN))
               if (!translateQuad(newQuad, -6.0F, reload, 10, 12, Direction.UP))
                   translateQuadFrom(newQuad, 6.0F, reload, 12, 17, Direction.UP);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 3.5F, 4.25F);
                scale = v3f(0.85F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                scale = v3f(0.55F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.5F, 0, 0);
                scale = v3f(0.45F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 0, -1.25F);
                scale = v3f(1.2F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
