package pellucid.ava.misc.renderers.models.mk20;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MK20Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, 8, 0),
            new Vector3f(-2.25F, 2.25F, 2.5F),
            new Vector3f(1.1F, 1.2F, 0.6F)
    );

    private static final float[] RUN_ROTATION = new float[]{-61, 74, 59};
    private static final float[] RUN_SCALE = new float[]{0.4F, 0.5F, 0.7F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-12, 3, 6.5F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-12.5F, 2.25F, 6.5F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-13, 2.75F, 6.5F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{52, -1, -18}, new float[]{-4.75F, 7.75F, 6.5F}, new float[]{0.5F, 0.5F, 0.65F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{20, 4, 0}, new float[]{-3F, 4F, 3F}, new float[]{1.1F, 1.2F, 0.7F});
    private static final Perspective RELOAD_3 = new Perspective(new Vector3f(26, 4, 0), RELOAD_2.translation.copy(), RELOAD_2.scale.copy());
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(5, RELOAD_1));
        add(new Animation(19, RELOAD_1));
        add(new Animation(26, RELOAD_2));
        add(new Animation(33, RELOAD_3));
        add(new Animation(37, RELOAD_2));
        add(new Animation(40, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{10, 0, 35}, new float[]{0, -0.65F, 0.05F}, new float[]{0.75F, 1, 0.75F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{10, 0, 35}, new float[]{-0.1F, -0.5F, -0.25F}, new float[]{0.75F, 1, 0.75F});
    private static final Perspective LEFT_HAND_RELOADING_2_FP = new Perspective(new float[]{10, 0, 35}, new float[]{-0.2F, -0.3F, -0.3F}, new float[]{0.65F, 1, 0.65F});
    private static final Perspective LEFT_HAND_RELOADING_3_FP = new Perspective(new float[]{10, 0, 35}, new float[]{-0.2F, -0.4F, -0.25F}, new float[]{0.65F, 1, 0.65F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(5, LEFT_HAND_RELOADING_FP));
        add(new Animation(11, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(13, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(17, LEFT_HAND_RELOADING_FP));
        add(new Animation(20, LEFT_HAND_RELOADING_FP));
        add(new Animation(26, LEFT_HAND_RELOADING_2_FP));
        add(new Animation(30, LEFT_HAND_RELOADING_3_FP));
        add(new Animation(40, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9F, 3.449F, 9.5F}, new float[]{1.1F, 1.1F, 0.05F});

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:mk20/mk20_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:mk20/mk20_handle#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:mk20/mk20_fire#inventory");

    public MK20Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getHandleQuads());
        quads.addAll(getMagazineQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getHandleQuads()
    {
        return this.get(getHandle(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 2.5F, reload, 26, 33, Direction.SOUTH))
                translateQuadFrom(newQuad, 2.5F, reload , 33, 35, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 6.0F, reload, 7, 12, Direction.DOWN))
                translateQuadTo(newQuad, 4.0F, reload, 7, 12, Direction.SOUTH);
            else if (translateQuad(newQuad, 6.0F, reload, 12, 15, Direction.DOWN))
                translateQuad(newQuad, 4.0F, reload, 12, 15, Direction.SOUTH);
            else if (translateQuadFrom(newQuad, 6.0F, reload, 15, 19, Direction.UP))
                translateQuadFrom(newQuad, 4.0F, reload, 15, 19, Direction.NORTH);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2.5F, 4.75F, 3.25F);
                scale = v3f(0.75F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                scale = v3f(0.45F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.5F, 0, 0);
                scale = v3f(0.4F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 0, -1.25F);
                scale = v3f(1.2F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}