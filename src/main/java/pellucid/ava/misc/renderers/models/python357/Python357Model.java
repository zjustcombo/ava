package pellucid.ava.misc.renderers.models.python357;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Python357Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(-2, -14, 0),
            new Vector3f(-5.75F, 6.0F, 6.0F),
            new Vector3f(0.8F, 0.8F, 0.65F)
    );

    private static final float[] RUN_ROTATION = new float[]{68, -7, 16};
    private static final float[] RUN_SCALE = new float[]{0.8F, 0.8F, 0.85F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-5.0F, 8.25F, 6.75F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-4.75F, 7.5F, 6.75F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-4.5F, 8.25F, 6.75F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD = new Perspective(new float[]{39, -16, 24}, new float[]{-6.0F, 7.75F, 6.0F}, new float[]{0.8F, 0.8F, 0.85F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{-1, -30, 31}, new float[]{-6.5F, 6.25F, 6.0F}, fArr(0.8F));
    private static final Perspective RELOAD_3 = new Perspective(new float[]{-17, -15, 15}, new float[]{-7.75F, 5.75F, 6.75F}, fArr(0.75F));
    private static final Perspective RELOAD_4 = new Perspective(new float[]{-19, -2, -20}, new float[]{-7.5F, 6.0F, 6.75F}, new float[]{0.65F, 0.65F, 0.6F});
    private static final Perspective RELOAD_5 = new Perspective(new float[]{4, -2, -5}, new float[]{-7.0F, 6.5F, 6.75F}, new float[]{0.65F, 0.65F, 0.6F});
    private static final Perspective RELOAD_6 = new Perspective(new float[]{-8, -2, -5}, new float[]{-7.0F, 6.0F, 6.75F}, new float[]{0.65F, 0.65F, 0.6F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(6, RELOAD));
        add(new Animation(25, RELOAD));
        add(new Animation(34, RELOAD_2));
        add(new Animation(38, RELOAD_2));
        add(new Animation(41, RELOAD_3));
        add(new Animation(46, RELOAD_3));
        add(new Animation(50, RELOAD_4));
        add(new Animation(54, RELOAD_4));
        add(new Animation(58, RELOAD_5));
        add(new Animation(60, RELOAD_6));
        add(new Animation(62, RELOAD_5));
        add(new Animation(70, ORIGINAL_FP_RIGHT));
    }};

    public static final Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{0.0F, 0, 25.0F}, new float[]{-0.15F, -0.6F, 0.15F}, new float[]{1, 1, 1});
    public static final Perspective RIGHT_HAND_RELOADING_FP_2 = new Perspective(new float[]{0.0F, 0, 25.0F}, new float[]{-0.45F, -0.35F, 0}, new float[]{1, 1, 1});
    public static final Perspective RIGHT_HAND_RELOADING_FP_3 = new Perspective(new float[]{0.0F, 0, 25.0F}, new float[]{-0.2F, -0.575F, -0.075F}, new float[]{0.85F, 0.85F, 0.85F});
    public static final Perspective RIGHT_HAND_RELOADING_FP_4 = new Perspective(new float[]{-10.0F, -10, 40}, new float[]{-0.05F, -0.575F, -0.1F}, new float[]{0.7F, 0.7F, 1});
    public static final Perspective RIGHT_HAND_RELOADING_FP_5 = new Perspective(new float[]{-10.0F, -10, 40}, new float[]{-0.05F, -0.575F, 0.025F}, new float[]{0.7F, 0.7F, 1});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP =  new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_RELOADING_FP));
        add(new Animation(3, RIGHT_HAND_RELOADING_FP_2));
        add(new Animation(4, RIGHT_HAND_RELOADING_FP_2));
        add(new Animation(7, RIGHT_HAND_RELOADING_FP));
        add(new Animation(28, RIGHT_HAND_RELOADING_FP));
        add(new Animation(30, RIGHT_HAND_RELOADING_FP_3));
        add(new Animation(38, RIGHT_HAND_RELOADING_FP_3));
        add(new Animation(40, RIGHT_HAND_RELOADING_FP));
        add(new Animation(53, RIGHT_HAND_RELOADING_FP));
        add(new Animation(56, RIGHT_HAND_RELOADING_FP_4));
        add(new Animation(63, RIGHT_HAND_RELOADING_FP_5));
        add(new Animation(70, RIGHT_HAND_RELOADING_FP));
    }};

    public static final ModelResourceLocation BULLETS = new ModelResourceLocation("ava:python357/python357_bullets#inventory");
    public static final ModelResourceLocation WHEEL = new ModelResourceLocation("ava:python357/python357_wheel#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:python357/python357_fire#inventory");

    public Python357Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
//        List<BakedQuad> quads2 = new ArrayList<>(Minecraft.getInstance().getModelManager().getModel(getWheel()).getQuads(null, null, rand));
//        rotateQuad(quads2, Direction.Axis.Z, 0.05F, new Vector3f(8F, 8.275F, 9.775F));
//        quads.addAll(quads2);
        quads.addAll(getWheelQuads());
        quads.addAll(getBulletsQuads());
        return quads;
    }

    protected ModelResourceLocation getBullets()
    {
        return BULLETS;
    }

    protected ModelResourceLocation getWheel()
    {
        return WHEEL;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getWheelQuads()
    {
        return rotateQuad(get(getWheel(), (newQuad) -> translateQuad(newQuad, 1.0F, reload, 6, 50, Direction.WEST, Direction.DOWN)), 360.0F, reload, 54, 70, Direction.Axis.Z, new Vector3f(8F, 8.275F, 9.775F));
    }

    protected List<BakedQuad> getBulletsQuads()
    {
        return get(getBullets(), (newQuad) -> {
            if (translateQuadTo(newQuad, 2.0F, reload, 6, 12, Direction.SOUTH))
                translateQuad(newQuad, 1.0F, reload, 6, 12, Direction.WEST, Direction.DOWN);
            else if (!translateQuad(newQuad, 2.0F, reload, 12, 30, Direction.SOUTH))
            {
                translateQuadFrom(newQuad, 2.0F, reload, 40, 50, Direction.NORTH);
                translateQuad(newQuad, 1.0F, reload, 30, 50, Direction.WEST, Direction.DOWN);
            }
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(BULLETS);
        ModelLoader.addSpecialModel(WHEEL);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = ONE_V.copy();
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-1.75F, 5.75F, 1.0F);
                scale = v3f(1.1F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, 0, -2.0F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(1.25F, 0.75F, 0);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(1.5F, 0.25F, -0.5F);
                scale = v3f(1.35F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
