package pellucid.ava.misc.renderers.models.mk18;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class Mk18KuyomonModel extends Mk18Model
{
    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:mk18/mk18_kuyo_mon_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:mk18/mk18_kuyo_mon_slide#inventory");

    public Mk18KuyomonModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }
}
