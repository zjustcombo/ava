package pellucid.ava.misc.renderers.models.x95r;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class X95RModel extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, 9, 0),
            new Vector3f(-3.5F, 1.75F, 0.25F),
            new Vector3f(2.5F, 2.5F, 1.5F)
    );

    private static final float[] RUN_ROTATION = new float[]{-45, 77, 24};
    private static final float[] RUN_SCALE = new float[]{0.75F, 0.85F, 1};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-7.75F, 4, 6.25F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-8.25F, 3.5F, 6.25F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-9.25F, 4, 6.25F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{66, 0, -32}, new float[]{9, 4.5F, -2}, new float[]{2.5F, 2.5F, 2.5F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{31, 2, 0}, new float[]{0, 0, -0.5F}, new float[]{3, 2.75F, 2});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(4, RELOAD_1));
        add(new Animation(23, RELOAD_1));
        add(new Animation(27, RELOAD_2));
        add(new Animation(32, RELOAD_2));
        add(new Animation(35, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 30}, new float[]{-0.4F, -0.6F, 0.2F}, new float[]{1, 1, 1});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 30}, new float[]{-0.35F, -0.2F, 0.075F}, new float[]{1, 1, 1});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(14, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(23, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(27, LEFT_HAND_RELOADING_FP));
        add(new Animation(35, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9.06F, 3.9425F, 10.0F}, new float[]{2.5F, 2.5F, 0.5F});

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:x95r/x95r_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:x95r/x95r_handle#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:x95r/x95r_fire#inventory");

    public X95RModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getHandleQuads());
        quads.addAll(getMagazineQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getHandleQuads()
    {
        return get(getHandle(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 1.5F, reload, 25, 31, Direction.SOUTH))
                translateQuadTo(newQuad, 1.5F, reload, 31, 33, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 6.0F, reload, 7, 12, Direction.DOWN))
                translateQuadTo(newQuad, 4.0F, reload, 7, 12, Direction.SOUTH);
            else if (translateQuad(newQuad, -6.0F, reload, 12, 15, Direction.UP))
                translateQuad(newQuad, -4.0F, reload, 12, 15, Direction.NORTH);
            else if (translateQuadFrom(newQuad, 6.0F, reload, 15, 19, Direction.UP))
                translateQuadFrom(newQuad, 4.0F, reload, 15, 19, Direction.NORTH);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2.5F, -0.25F, 6);
                scale = v3f(1.5F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -2.5F, 0);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(-0.5F, 0.75F, 0);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 1, 0  );
                scale = v3f(2);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
