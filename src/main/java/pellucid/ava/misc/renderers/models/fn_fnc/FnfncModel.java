package pellucid.ava.misc.renderers.models.fn_fnc;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FnfncModel extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(1, 8, 0),
            new Vector3f(-3.25F, 4.25F, 2.5F),
            new Vector3f(1.25F, 1, 0.45F)
    );

    private static final float[] RUN_ROTATION = new float[]{-27, 54, 35};
    private static final float[] RUN_SCALE = new float[]{0.8F, 1, 1};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-12.25F, 2.75F, 2}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-12.75F, 2.25F, 2}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-13.25F, 2.75F, 2}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    protected static final float[] RELOAD_ROTATION = new float[]{39, 7, 28};
    protected static final float[] RELOAD_TRANSLATION = new float[]{-2.75F, 5.25F, 1.75F};
    protected static final float[] RELOAD_SCALE = new float[]{1.25F, 1, 0.6F};
    private static final Perspective RELOAD_1 = new Perspective(new float[]{26, 7, -9}, RELOAD_TRANSLATION, RELOAD_SCALE);
    private static final Perspective RELOAD_2 = new Perspective(RELOAD_ROTATION, RELOAD_TRANSLATION, RELOAD_SCALE);
    private static final Perspective RELOAD_3 = new Perspective(RELOAD_ROTATION, new float[]{-2.75F, 4.25F, 2.75F}, RELOAD_SCALE);
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(3, RELOAD_1));
        add(new Animation(20, RELOAD_1));
        add(new Animation(25, RELOAD_2));
        add(new Animation(28, RELOAD_3));
        add(new Animation(31, RELOAD_2));
        add(new Animation(35, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{-17, 0, 50}, new float[]{-0.2F, -0.6F, 0.45F}, new float[]{0.75F, 0.75F, 1});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{-17, 0, 50}, new float[]{-0.2F, 0, 0.075F}, new float[]{0.75F, 0.75F, 1});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(16, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(22, LEFT_HAND_RELOADING_FP));
        add(new Animation(31, LEFT_HAND_RELOADING_FP));
        add(new Animation(35, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective RIGHT_HAND_ORIGINAL_FP = new Perspective(new float[]{-15, 0, 0}, new float[]{-0.25F, -0.4F, 0.275F}, new float[]{0.85F, 0.85F, 1});
    private static final Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{-15, 0, 0}, new float[]{-0.25F, -0.2F, 0.075F}, new float[]{0.85F, 0.85F, 1});
    public static final ArrayList<Animation> RIGHT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(16, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(24, RIGHT_HAND_RELOADING_FP));
        add(new Animation(35,  RIGHT_HAND_ORIGINAL_FP));
    }};

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:fn_fnc/fn_fnc_magazine#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:fn_fnc/fn_fnc_fire#inventory");

    public FnfncModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getMagazineQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 6.0F, reload, 7, 12, Direction.DOWN))
                translateQuadTo(newQuad, 4.0F, reload, 7, 12, Direction.SOUTH);
            else if (translateQuad(newQuad, -6.0F, reload, 12, 14, Direction.UP))
                translateQuad(newQuad, -4.0F, reload, 12, 14, Direction.NORTH);
            else if (translateQuadFrom(newQuad, 6.0F, reload, 14, 19, Direction.UP))
                translateQuadFrom(newQuad, 4.0F, reload, 14, 19, Direction.NORTH);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2.5F, 3.5F, 5);
                scale = v3f(0.875F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -1.75F, -0.75F);
                scale = v3f(0.55F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.75F, 0.75F, 0);
                scale = v3f(0.4F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 2, -1.25F);
                scale = v3f(1.1F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
