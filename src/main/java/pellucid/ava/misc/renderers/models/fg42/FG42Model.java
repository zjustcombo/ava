package pellucid.ava.misc.renderers.models.fg42;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FG42Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, -2, 0),
            new Vector3f(-3.25F, 5.5F, 2.25F),
            new Vector3f(1, 1.2F, 0.45F)
    );

    private static final float[] RUN_ROTATION = new float[]{-20, 60, 35};
    private static final float[] RUN_SCALE = new float[]{0.4F, 0.6F, 0.5F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-10.75F, 6.25F, 7.75F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-11.25F, 6, 7.75F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-11.75F, 6.25F, 7.75F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{5, 19, 12}, new float[]{-3, 4.5F, -0.25F}, new float[]{1, 1.1F, 0.55F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{16, 26, 12}, new float[]{-6.5F, 4.5F, 0.25F}, new float[]{1, 1.1F, 0.55F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(4, RELOAD_1));
        add(new Animation(23, RELOAD_1));
        add(new Animation(27, RELOAD_2));
        add(new Animation(32, RELOAD_2));
        add(new Animation(40, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective RIGHT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, -35}, new float[]{-0.45F, -0.75F, -0.15F}, new float[]{0.7F, 0.7F, 1});
    private static final Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, -35}, new float[]{-0.45F, -0.2F, 0}, new float[]{0.7F, 0.7F, 1});
    public static final ArrayList<Animation> RIGHT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(22, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(29, RIGHT_HAND_RELOADING_FP));
        add(new Animation(40,  RIGHT_HAND_ORIGINAL_FP));
    }};

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:fg42/fg42_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:fg42/fg42_handle#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:fg42/fg42_fire#inventory");

    public FG42Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        if (this.fire != 0)
            quads.addAll(getFireQuads(state, rand));
        quads.addAll(getHandleQuads());
        quads.addAll(getMagazineQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getHandleQuads()
    {
        return get(getHandle(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 2.5F, reload, 26, 32, Direction.SOUTH))
                translateQuadFrom(newQuad, 2.5F, reload, 32, 34, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 4.0F, reload, 5, 10, Direction.DOWN, Direction.SOUTH))
                if (!translateQuad(newQuad, -4.0F, reload, 10, 15, Direction.UP, Direction.NORTH))
                    translateQuadFrom(newQuad, 4.0F, reload, 15, 19, Direction.UP, Direction.NORTH);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 4F, 6.75F);
                scale = v3f(0.85F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -1F, 1.5F);
                scale = v3f(0.65F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 0.75F, 0);
                scale = v3f(0.45F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(-0.75F, 1.25F, -1);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
