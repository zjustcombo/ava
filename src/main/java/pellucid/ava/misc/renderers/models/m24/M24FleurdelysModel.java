package pellucid.ava.misc.renderers.models.m24;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class M24FleurdelysModel extends M24Model
{
    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:m24/m24_fleur_de_lys_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:m24/m24_fleur_de_lys_slide#inventory");

    public M24FleurdelysModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }
}