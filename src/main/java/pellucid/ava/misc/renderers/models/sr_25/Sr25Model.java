package pellucid.ava.misc.renderers.models.sr_25;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sr25Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(-3F, 0, 0),
            new Vector3f(-4.25F, 4.5F, 3.5F),
            new Vector3f(1.1F, 1F, 0.5F)
    );

    private static final float[] RUN_ROTATION = new float[]{-30, 50, 30};
    private static final float[] RUN_SCALE = new float[]{0.85F, 1, 1};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-12.5F, 2.75F, 4.25F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-13, 2.25F, 4.25F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-13.5F, 2.75F, 4.25F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{13, -20, -17}, new float[]{-2.75F, 5.5F, 3.25F}, new float[]{1, 1, 0.75F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{13, -20, -37}, new float[]{-2.75F, 5.5F, 3.25F}, new float[]{1, 1, 0.75F});
    private static final Perspective RELOAD_3 = new Perspective(new float[]{-9, 0, 11}, new float[]{-5.25F, 3.5F, 3.25F}, new float[]{1, 1, 0.5F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(3, RELOAD_1));
        add(new Animation(12, RELOAD_1));
        add(new Animation(14, RELOAD_2));
        add(new Animation(20, RELOAD_2));
        add(new Animation(26, RELOAD_3));
        add(new Animation(30, RELOAD_3));
        add(new Animation(34, ORIGINAL_FP_RIGHT));
        add(new Animation(35, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{20, 0, 30}, new float[]{-0.4F, -0.65F, -0.15F}, new float[]{1, 1, 1});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{20, 0, 30}, new float[]{-0.4F, 0, 0}, new float[]{1, 1, 1});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(16, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(18, LEFT_HAND_RELOADING_FP));
        add(new Animation(30, LEFT_HAND_RELOADING_FP));
        add(new Animation(32, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(35, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective RIGHT_HAND_ORIGINAL_FP = new Perspective(new float[]{10, 0, 0}, new float[]{-0.05F, -0.5F, -0.3F}, new float[]{0.45F, 0.45F, 1});
    private static final Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{10, 0, 0}, new float[]{-0.025F, -0.35F, -0.25F}, new float[]{0.45F, 0.45F, 1});
    public static final ArrayList<Animation> RIGHT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(14, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(18, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(22, RIGHT_HAND_RELOADING_FP));
        add(new Animation(35, RIGHT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9, 5.5F, 3.5F}, new float[]{1.1F, 1, 0.5F});

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:sr_25/sr_25_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:sr_25/sr_25_slide#inventory");

    public Sr25Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getMagazineQuads());
        quads.addAll(getSlideQuads());
        return quads;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getSlideQuads()
    {
        return get(getSlide(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 1.5F, reload, 27, 31, Direction.SOUTH))
                translateQuadFrom(newQuad, 1.5F, reload, 31, 35, Direction.NORTH);
        });
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 3.0F, reload, 3, 8, Direction.DOWN))
                if (!translateQuad(newQuad, 3.0F, reload, 8, 10, Direction.UP))
                    translateQuadFrom(newQuad, 3.0F, reload, 10, 15, Direction.UP);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 3.75F, 6.25F);
                scale = v3f(1);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -2.75F, 0);
                scale = v3f(0.75F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                scale = v3f(0.45F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 0, -0.75F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
