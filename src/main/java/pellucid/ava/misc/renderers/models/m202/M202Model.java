package pellucid.ava.misc.renderers.models.m202;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class M202Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(-12F, 0, 0),
            new Vector3f(1.5F, 3F, 5.25F),
            new Vector3f(1, 1.1F, 0.75F)
    );

    private static final float[] RUN_ROTATION = new float[]{-12, 68, 0};
    private static final float[] RUN_SCALE = fArr(0.5F);
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-4.75F, 3.5F, 6F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-5.25F, 3F, 6F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>()
    {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-5.75F, 3.5F, 6F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{-38F, 0, 0}, new float[]{-2, 3, 5.25F}, new float[]{1, 1.1F, 0.75F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>()
    {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(5, RELOAD_1));
        add(new Animation(25, RELOAD_1));
        add(new Animation(30, ORIGINAL_FP_RIGHT));
    }};

    public static final ModelResourceLocation ROCKETS = new ModelResourceLocation("ava:m202/m202_rocket#inventory");

    public M202Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
        {
            return quads;
        }
        quads.addAll(getRocketsQuads());
        return quads;
    }

    protected ModelResourceLocation getRockets()
    {
        return ROCKETS;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getRocketsQuads()
    {
        return get(getRockets(), (newQuad) ->
        {
            if (translateQuadTo(newQuad, 4.0F, reload, 5, 12, Direction.DOWN))
            {
                translateQuadTo(newQuad, 6.0F, reload, 5, 12, Direction.SOUTH);
            }
            else if (translateQuad(newQuad, 4.0F, reload, 12, 18, Direction.DOWN))
            {
                translateQuad(newQuad, 6.0F, reload, 12, 18, Direction.SOUTH);
            }
            else if (translateQuadFrom(newQuad, 4.0F, reload, 18, 25, Direction.UP))
            {
                translateQuadFrom(newQuad, 6.0F, reload, 18, 25, Direction.NORTH);
            }
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(ROCKETS);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-1.5F, 4F, 8F);
                scale = v3f(0.85F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                translation = new Vector3f(0, 0, 5.5F);
                scale = v3f(0.75F);
                break;
            case GUI:
                rotation = new Vector3f(98F, -49F, 88F);
                translation = new Vector3f(-1.75F, -2F, 0.0F);
                scale = v3f(0.4F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                scale = new Vector3f(-6.5F, 0.0F, 0.0F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
        {
            transformationMatrix.push(mat);
        }
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
