package pellucid.ava.misc.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.misc.cap.IWorldData;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.commands.RecoilRefundTypeCommand;

import java.util.function.Supplier;

public class SyncCapabilityDataMessage
{
    private final boolean shouldRenderCrosshair;
    private final boolean friendlyFire;
    private final boolean reducedFriendlyFire;
    private final boolean doGlassBreak;
    private final float mobDropsKitsChance;
    private final RecoilRefundTypeCommand.RefundType recoilRefundType;

    public SyncCapabilityDataMessage(IWorldData world)
    {
        this(world.getRecoilRefundType(), world.shouldRenderCrosshair(), world.isFriendlyFireAllowed(), world.isFriendlyFireReduced(), world.isGlassDestroyable(), world.getMobDropsKitsChance());
    }

    public SyncCapabilityDataMessage(RecoilRefundTypeCommand.RefundType recoilRefundType, boolean shouldRenderCrosshair, boolean friendlyFire, boolean reducedFriendlyFire, boolean doGlassBreak, float mobDropsKitsChance)
    {
        this.recoilRefundType = recoilRefundType;
        this.shouldRenderCrosshair = shouldRenderCrosshair;
        this.friendlyFire = friendlyFire;
        this.reducedFriendlyFire = reducedFriendlyFire;
        this.doGlassBreak = doGlassBreak;
        this.mobDropsKitsChance = mobDropsKitsChance;
    }

    public static void encode(SyncCapabilityDataMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeString(msg.recoilRefundType.toString()).writeBoolean(msg.shouldRenderCrosshair).writeBoolean(msg.friendlyFire).writeBoolean(msg.reducedFriendlyFire).writeBoolean(msg.doGlassBreak).writeFloat(msg.mobDropsKitsChance);
    }

    public static SyncCapabilityDataMessage decode(PacketBuffer packetBuffer)
    {
        return new SyncCapabilityDataMessage(RecoilRefundTypeCommand.RefundType.valueOf(packetBuffer.readString(32767)), packetBuffer.readBoolean(), packetBuffer.readBoolean(), packetBuffer.readBoolean(), packetBuffer.readBoolean(), packetBuffer.readFloat());
    }

    public static void handle(SyncCapabilityDataMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
                DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message)));
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(SyncCapabilityDataMessage message)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
        {
            IWorldData w = WorldData.getCap(player.world);
            w.setShouldRenderCrosshair(message.shouldRenderCrosshair);
            w.setFriendlyFire(message.friendlyFire);
            w.setReducedFriendlyFire(message.reducedFriendlyFire);
            w.setGlassDestroyable(message.doGlassBreak);
            w.setMobDropsKitsChance(message.mobDropsKitsChance);
            w.setRecoilRefundType(message.recoilRefundType);
        }
    }
}
