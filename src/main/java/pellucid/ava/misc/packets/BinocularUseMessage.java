package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.miscs.Binocular;

import java.util.function.Supplier;

public class BinocularUseMessage
{
    public BinocularUseMessage() {}
    public static void encode(BinocularUseMessage msg, PacketBuffer packetBuffer) {}

    public static BinocularUseMessage decode(PacketBuffer packetBuffer)
    {
        return new BinocularUseMessage();
    }

    public static void handle(BinocularUseMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if (stack.getItem() instanceof Binocular)
                {
                    Binocular binocular = (Binocular) stack.getItem();
                    if (binocular.firable(player, stack))
                        binocular.fire(player.getEntityWorld(), player, stack);
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
