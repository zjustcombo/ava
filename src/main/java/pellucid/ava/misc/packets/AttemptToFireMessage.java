package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.function.Supplier;

public class AttemptToFireMessage
{
    public AttemptToFireMessage() {}

    public static void encode(AttemptToFireMessage msg, PacketBuffer packetBuffer) {}

    public static AttemptToFireMessage decode(PacketBuffer packetBuffer)
    {
        return new AttemptToFireMessage();
    }

    public static void handle(AttemptToFireMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if (stack.getItem() instanceof AVAItemGun)
                {
                    AVAItemGun gun = (AVAItemGun) stack.getItem();
                    CompoundNBT compound = gun.initTags(stack);
                    if (gun.firable(player, stack))
                    {
                        gun.fire(player.getEntityWorld(), player, stack);
                        PlayerAction.getCap(player).setFiring(true);
                        AVAPackets.INSTANCE.sendTo(new FiringMessage(true, false), player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
                    }
                    else if (compound.getInt("ticks") == 0)
                    {
                        PlayerAction.getCap(player).setFiring(false);
                        AVAPackets.INSTANCE.sendTo(new FiringMessage(false, false), player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
                    }
                    if (compound.getInt("ammo") < 1 && gun.reloadable(player, stack))
                        gun.preReload(player, stack);
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
