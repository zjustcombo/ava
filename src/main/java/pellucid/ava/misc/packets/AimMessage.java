package pellucid.ava.misc.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.function.Supplier;

public class AimMessage
{
    private boolean aiming;
    private boolean toServer;

    public AimMessage(boolean aiming, boolean toServer)
    {
        this.aiming = aiming;
        this.toServer = toServer;
    }

    public static void encode(AimMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeBoolean(msg.aiming).writeBoolean(msg.toServer);
    }

    public static AimMessage decode(PacketBuffer packetBuffer)
    {
        return new AimMessage(packetBuffer.readBoolean(), packetBuffer.readBoolean());
    }

    public static void handle(AimMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
        {
            if (message.toServer)
            {
                ServerPlayerEntity player = ctx.get().getSender();
                if (player != null)
                {
                    ItemStack stack = player.getHeldItemMainhand();
                    if (stack.getItem() instanceof AVAItemGun)
                    {
                        if (((AVAItemGun) stack.getItem()).isReloadInteractable() && message.aiming)
                            ((AVAItemGun) stack.getItem()).initTags(stack).putInt("interact", 1);
                        IPlayerAction capability = PlayerAction.getCap(player);
                        capability.setAiming(message.aiming);
                        player.getEntityWorld().playSound(null, player.getPosX(), player.getPosY(), player.getPosZ(), ((AVAItemGun) player.getHeldItemMainhand().getItem()).getScopeSound(), SoundCategory.PLAYERS, 0.75F, 1.0F);
                    }
                }
            }
            else
                DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message.aiming));
        });
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(boolean aiming)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
        {
            ItemStack stack = player.getHeldItemMainhand();
            if (stack.getItem() instanceof AVAItemGun)
            {
                IPlayerAction capability = PlayerAction.getCap(player);
                capability.setAiming(aiming);
            }
        }
    }
}
