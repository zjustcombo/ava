package pellucid.ava.misc.packets;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import pellucid.ava.AVA;

public class AVAPackets
{
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(new ResourceLocation(AVA.MODID, "main"),
            () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals
    );

    public static void registerAll(boolean isDedicatedServer)
    {
        int id = 0;
        INSTANCE.registerMessage(id++, AttemptToFireMessage.class, AttemptToFireMessage::encode, AttemptToFireMessage::decode, AttemptToFireMessage::handle);
        INSTANCE.registerMessage(id++, AttemptToReloadMessage.class, AttemptToReloadMessage::encode, AttemptToReloadMessage::decode, AttemptToReloadMessage::handle);
        INSTANCE.registerMessage(id++, FiringMessage.class, FiringMessage::encode, FiringMessage::decode, FiringMessage::handle);
        INSTANCE.registerMessage(id++, ThrowGrenadeMessage.class, ThrowGrenadeMessage::encode, ThrowGrenadeMessage::decode, ThrowGrenadeMessage::handle);
//        if (!isDedicatedServer)
//        {
            INSTANCE.registerMessage(id++, AimMessage.class, AimMessage::encode, AimMessage::decode, AimMessage::handle);
            INSTANCE.registerMessage(id++, FlashMessage.class, FlashMessage::encode, FlashMessage::decode, FlashMessage::handle);
//        }
        INSTANCE.registerMessage(id++, PlaySoundMessage.class, PlaySoundMessage::encode, PlaySoundMessage::decode, PlaySoundMessage::handle);
        INSTANCE.registerMessage(id++, RadioMessage.class, RadioMessage::encode, RadioMessage::decode, RadioMessage::handle);
        INSTANCE.registerMessage(id++, SyncCapabilityDataMessage.class, SyncCapabilityDataMessage::encode, SyncCapabilityDataMessage::decode, SyncCapabilityDataMessage::handle);
        INSTANCE.registerMessage(id++, CraftMessage.class, CraftMessage::encode, CraftMessage::decode, CraftMessage::handle);
        INSTANCE.registerMessage(id++, PaintMessage.class, PaintMessage::encode, PaintMessage::decode, PaintMessage::handle);
        INSTANCE.registerMessage(id++, UAVMessage.class, UAVMessage::encode, UAVMessage::decode, UAVMessage::handle);
        INSTANCE.registerMessage(id++, BinocularAimMessage.class, BinocularAimMessage::encode, BinocularAimMessage::decode, BinocularAimMessage::handle);
        INSTANCE.registerMessage(id++, BinocularUseMessage.class, BinocularUseMessage::encode, BinocularUseMessage::decode, BinocularUseMessage::handle);
        INSTANCE.registerMessage(id++, PlaySoundToClientMessage.class, PlaySoundToClientMessage::encode, PlaySoundToClientMessage::decode, PlaySoundToClientMessage::handle);
        INSTANCE.registerMessage(id++, AttemptToMeleeMessage.class, AttemptToMeleeMessage::encode, AttemptToMeleeMessage::decode, AttemptToMeleeMessage::handle);
        INSTANCE.registerMessage(id++, PresetMessage.class, PresetMessage::encode, PresetMessage::decode, PresetMessage::handle);
    }
}
