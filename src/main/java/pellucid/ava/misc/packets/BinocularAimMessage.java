package pellucid.ava.misc.packets;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.miscs.Binocular;
import pellucid.ava.misc.AVASounds;

import java.util.function.Supplier;

public class BinocularAimMessage
{
    private boolean aim;

    public BinocularAimMessage(boolean aim)
    {
        this.aim = aim;
    }

    public static void encode(BinocularAimMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeBoolean(msg.aim);
    }

    public static BinocularAimMessage decode(PacketBuffer packetBuffer)
    {
        return new BinocularAimMessage(packetBuffer.readBoolean());
    }

    public static void handle(BinocularAimMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                Item item = player.getHeldItemMainhand().getItem();
                if (item instanceof Binocular)
                {
                    player.getEntityWorld().playSound(null, player.getPosX(), player.getPosY(), player.getPosZ(), AVASounds.DEFAULT_AIM, SoundCategory.PLAYERS, 0.75F, 1.1F);
                    CompoundNBT compound = ((Binocular) item).initTags(player.getHeldItemMainhand());
                    compound.putBoolean("aiming", message.aim);
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
