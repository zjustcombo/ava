package pellucid.ava.misc.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.function.Supplier;

public class FlashMessage
{
    private final int duration;

    public FlashMessage(int duration)
    {
        this.duration = duration;
    }

    public static void encode(FlashMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeInt(msg.duration);
    }

    public static FlashMessage decode(PacketBuffer packetBuffer)
    {
        return new FlashMessage(packetBuffer.readInt());
    }

    public static void handle(FlashMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message.duration)));
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(int duration)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
            PlayerAction.getCap(player).setFlashDuration(duration);
    }
}
