package pellucid.ava.misc.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.function.Supplier;

public class FiringMessage
{
    private boolean firing;
    private boolean toServer;

    public FiringMessage(boolean firing, boolean toServer)
    {
        this.firing = firing;
        this.toServer = toServer;
    }

    public static void encode(FiringMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeBoolean(msg.firing).writeBoolean(msg.toServer);
    }

    public static FiringMessage decode(PacketBuffer packetBuffer)
    {
        return new FiringMessage(packetBuffer.readBoolean(), packetBuffer.readBoolean());
    }

    public static void handle(FiringMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
        {
            if (message.toServer)
            {
                ServerPlayerEntity player = ctx.get().getSender();
                if (player != null)
                {
                    IPlayerAction capability = PlayerAction.getCap(player);
                    capability.setFiring(message.firing);
                }
            }
            else
                DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message.firing));
        });
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(boolean firing)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
            PlayerAction.getCap(player).setFiring(firing);
    }
}
