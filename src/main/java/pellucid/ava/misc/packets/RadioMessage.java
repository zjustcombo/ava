package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class RadioMessage
{
    private String pre;
    private int num;

    public RadioMessage(String pre, int num)
    {
        this.pre = pre;
        this.num = num;
    }

    public static void encode(RadioMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeString(msg.pre).writeInt(msg.num);
    }

    public static RadioMessage decode(PacketBuffer packetBuffer)
    {
        return new RadioMessage(packetBuffer.readString(), packetBuffer.readInt());
    }

    public static void handle(RadioMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity sender = ctx.get().getSender();
            if (sender != null)
                for (ServerPlayerEntity player : sender.getServerWorld().getPlayers())
                    if (player.isOnSameTeam(sender))
                        player.sendMessage(getMessage(message.pre + message.num));
        });
        ctx.get().setPacketHandled(true);
    }

    public static TranslationTextComponent getMessage(String text)
    {
        TranslationTextComponent component = new TranslationTextComponent(text);
        component.setStyle(new Style().setColor(TextFormatting.GOLD));
        return component;
    }
}
