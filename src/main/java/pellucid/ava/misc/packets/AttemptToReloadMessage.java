package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.function.Supplier;

public class AttemptToReloadMessage
{
    private AttemptToReloadMessage() {}

    public static AttemptToReloadMessage create()
    {
        return new AttemptToReloadMessage();
    }

    public static void encode(AttemptToReloadMessage msg, PacketBuffer packetBuffer) {}

    public static AttemptToReloadMessage decode(PacketBuffer packetBuffer)
    {
        return new AttemptToReloadMessage();
    }

    public static void handle(AttemptToReloadMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if (stack.getItem() instanceof AVAItemGun)
                {
                    AVAItemGun gun = (AVAItemGun) stack.getItem();
                    if (gun.reloadable(player, stack))
                    {
                        gun.preReload(player, stack);
                        PlayerAction.getCap(player).setAiming(false);
                    }
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
