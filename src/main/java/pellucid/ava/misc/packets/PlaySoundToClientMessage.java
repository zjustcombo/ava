package pellucid.ava.misc.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class PlaySoundToClientMessage
{
    private final String sound, category;
    private final double x, y, z;
    private final float volume, pitch;
    private int moving = -1;

    public PlaySoundToClientMessage(SoundEvent sound, double x, double y, double z)
    {
        this(sound, x, y, z, 1.0F, 1.0F);
    }

    public PlaySoundToClientMessage(SoundEvent sound, double x, double y, double z, float volume, float pitch)
    {
        this(sound.getRegistryName().toString(), x, y, z, volume, pitch);
    }

    public PlaySoundToClientMessage(String sound, double x, double y, double z, float volume, float pitch)
    {
        this(sound, SoundCategory.PLAYERS, x, y, z, volume, pitch);
    }

    public PlaySoundToClientMessage(String sound, SoundCategory category, double x, double y, double z, float volume, float pitch)
    {
        this(sound, category.toString(), x, y, z, volume, pitch);
    }

    public PlaySoundToClientMessage(String sound, String category, double x, double y, double z, float volume, float pitch)
    {
        this.sound = sound;
        this.category = category;
        this.x = x;
        this.y = y;
        this.z = z;
        this.volume = volume;
        this.pitch = pitch;
    }

    public PlaySoundToClientMessage setMoving(int entityID)
    {
        this.moving = entityID;
        return this;
    }

    public static void encode(PlaySoundToClientMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeString(msg.sound).writeString(msg.category).writeDouble(msg.x).writeDouble(msg.y).writeDouble(msg.z).writeFloat(msg.volume).writeFloat(msg.pitch).writeInt(msg.moving);
    }

    public static PlaySoundToClientMessage decode(PacketBuffer b)
    {
        return new PlaySoundToClientMessage(b.readString(32767), b.readString(32767), b.readDouble(), b.readDouble(), b.readDouble(), b.readFloat(), b.readFloat()).setMoving(b.readInt());
    }

    public static void handle(PlaySoundToClientMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message)));
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(PlaySoundToClientMessage message)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(message.sound));
        if (player != null && sound != null)
        {
            Entity entity = player.world.getEntityByID(message.moving);
            SoundCategory category = SoundCategory.valueOf(message.category);
            if (entity == null)
                player.world.playSound(message.x, message.y, message.z, sound, category, message.volume, message.pitch, false);
            else
                player.world.playMovingSound(player, entity, sound, category, message.volume, message.pitch);
        }
    }
}
