package pellucid.ava.items.init;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.miscs.ICustomModel;
import pellucid.ava.items.miscs.MeleeWeapon;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.meleemodels.field_knife.FieldKnifeModel;

import java.util.ArrayList;

public class MeleeWeapons
{
    public static Item FIELD_KNIFE = null;

    public static final ArrayList<Item> ITEM_MELEE_WEAPONS = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                FIELD_KNIFE = new MeleeWeapon(new Recipe().addItem(Items.IRON_INGOT, 5).addItem(Items.COAL, 3), 35, 70, 0.5F, 1.0F, 1.3F, 1.7F, 0.0225F).setRegistryName("field_knife")
        );
        DistExecutor.runWhenOn(Dist.CLIENT, () -> MeleeWeapons::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((ICustomModel) FIELD_KNIFE).setCustomModel((origin) -> new Model(origin, FieldKnifeModel::new));
    }
}
