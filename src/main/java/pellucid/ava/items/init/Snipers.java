package pellucid.ava.items.init;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVAManual;
import pellucid.ava.items.guns.AVASingle;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAItemGroups;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.models.m24.M24FleurdelysModel;
import pellucid.ava.misc.renderers.models.m24.M24Model;
import pellucid.ava.misc.renderers.models.mosin_nagant.MosinNagantModel;
import pellucid.ava.misc.renderers.models.sr_25.Sr25KnutModel;
import pellucid.ava.misc.renderers.models.sr_25.Sr25Model;

import java.util.ArrayList;

public class Snipers
{
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */

    //stability -= 70
    public static Item MOSIN_NAGANT_AMMO = null;
    private static final AVAItemGun.Properties MOSIN_NAGANT_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SNIPER).damage(125).range(96).accuracy(89).stability(78 - 65).speed(1).maxAmmo(5).mobility(100.0F - 6.0F).reloadTime(0.95F).requireAim()
            .shootSound(AVASounds.MOSIN_NAGANT_SHOOT).reloadSound(AVASounds.MOSIN_NAGANT_RELOAD).scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT).noCrosshair().recoilRefund(0.25F);

    public static Item MOSIN_NAGANT = null;
    public static Item MOSIN_NAGANT_SUMIRE = null;

    public static Item M24_MAGAZINE = null;
    private static final AVAItemGun.Properties M24_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SNIPER).reloadSound(AVASounds.M24_RELOAD).damage(126.7F).range(94).accuracy(87.8F).stability(69.9F - 65).speed(1).maxAmmo(5).mobility(99.9F - 6.0F).reloadTime(1.5F)
            .shootSound(AVASounds.M24_SHOOT).scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT).requireAim().noCrosshair().recoilRefund(0.25F);

    public static Item M24 = null;
    public static Item M24_FLEUR_DE_LYS = null;

    public static Item SR_25_MAGAZINE = null;
    private static final AVAItemGun.Properties SR_25_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SNIPER_SEMI).reloadSound(AVASounds.SR_25_RELOAD).damage(97).range(89).accuracy(92.5F).stability(86.1F - 70).speed(2).maxAmmo(20).mobility(94.3F - 6.0F).reloadTime(1.5F)
            .shootSound(AVASounds.SR_25_SHOOT).scopeType(AVAItemGun.ScopeTypes.SR25).requireAim().noCrosshair().manual().recoilRefund(0.25F);

    public static Item SR_25 = null;
    public static Item SR_25_KNUT = null;

    public static ArrayList<Item> ITEM_SNIPERS = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                MOSIN_NAGANT_AMMO = new Ammo(new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT).addItem(Items.GOLD_NUGGET).setResultCount(8)).setRegistryName("mosin_nagant_ammo"),
                MOSIN_NAGANT = new AVAManual(MOSIN_NAGANT_P.magazineType(MOSIN_NAGANT_AMMO), new Recipe().addItem(Items.IRON_INGOT, 20).addItem(Items.ACACIA_PLANKS, 24).addItem(Items.GLASS_PANE)).setRegistryName("mosin_nagant"),
                MOSIN_NAGANT_SUMIRE = new AVAManual(MOSIN_NAGANT_P.skinned((AVAItemGun) MOSIN_NAGANT), new Recipe().addItem(MOSIN_NAGANT).mergeIngredients(SharedRecipes.SUMIRE)).setRegistryName("mosin_nagant_sumire"),

                M24_MAGAZINE = new Ammo(new Item.Properties().group(AVAItemGroups.MAIN).maxDamage(5), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("m24_magazine"),
                M24 = new AVASingle(M24_P.magazineType(M24_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 12).addItem(Items.OAK_LEAVES, 5).addItem(Items.GLASS_PANE)).setRegistryName("m24"),
                M24_FLEUR_DE_LYS = new AVASingle(M24_P.skinned((AVAItemGun) M24), new Recipe().addItem(M24).mergeIngredients(SharedRecipes.FLEUR_DE_LYS)).setRegistryName("m24_fleur_de_lys"),

                SR_25_MAGAZINE = new Ammo(new Item.Properties().group(AVAItemGroups.MAIN).maxDamage(20), new Recipe().addItem(Items.GUNPOWDER, 2).addItem(Items.IRON_INGOT).addItem(Items.IRON_NUGGET, 3), true).setRegistryName("sr_25_magazine"),
                SR_25 = new AVAItemGun(SR_25_P.magazineType(SR_25_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 10).addItem(Items.COAL, 3).addItem(Items.GLASS_PANE)).setRegistryName("sr_25"),
                SR_25_KNUT = new AVAItemGun(SR_25_P.skinned((AVAItemGun) SR_25), new Recipe().addItem(SR_25).mergeIngredients(SharedRecipes.KNUT)).setRegistryName("sr_25_knut")
        );
        DistExecutor.runWhenOn(Dist.CLIENT, () -> Snipers::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((AVAItemGun) MOSIN_NAGANT).setCustomModel((origin) -> new Model(origin, MosinNagantModel::new));
        ((AVAItemGun) MOSIN_NAGANT_SUMIRE).setCustomModel((origin) -> new Model(origin, MosinNagantModel::new));
        ((AVAItemGun) M24).setCustomModel((origin) -> new Model(origin, M24Model::new));
        ((AVAItemGun) M24_FLEUR_DE_LYS).setCustomModel((origin) -> new Model(origin, M24FleurdelysModel::new));
        ((AVAItemGun) SR_25).setCustomModel((origin) -> new Model(origin, Sr25Model::new));
        ((AVAItemGun) SR_25_KNUT).setCustomModel((origin) -> new Model(origin, Sr25KnutModel::new));
    }
}
