package pellucid.ava.items.init;

import net.minecraft.item.Items;
import pellucid.ava.items.miscs.Recipe;

public class SharedRecipes
{
    public static final Recipe FLEUR_DE_LYS = new Recipe().addItem(Items.LIGHT_GRAY_DYE, 4).addItem(Items.BLUE_DYE, 2).addItem(Items.BLACK_DYE, 2).addItem(Items.YELLOW_DYE, 4);
    public static final Recipe SUMIRE = new Recipe().addItem(Items.PURPLE_DYE, 5).addItem(Items.WHITE_DYE, 2).addItem(Items.YELLOW_DYE, 3);
    public static final Recipe DREAMCATCHER = new Recipe().addItem(Items.BLACK_DYE, 7).addItem(Items.YELLOW_DYE, 7).addItem(Items.WHITE_DYE, 4);
    public static final Recipe FULLMOON = new Recipe().addItem(Items.BROWN_DYE, 10).addItem(Items.YELLOW_DYE, 7);
    public static final Recipe FROST = new Recipe().addItem(Items.WHITE_DYE, 7).addItem(Items.SNOW_BLOCK, 2);
    public static final Recipe SNOWFALL = new Recipe().addItem(Items.WHITE_DYE, 5).addItem(Items.ICE);
    public static final Recipe AIR_WARFARE = new Recipe().addItem(Items.LIGHT_BLUE_DYE, 7).addItem(Items.BLUE_DYE, 5).addItem(Items.WHITE_DYE, 3);
    public static final Recipe KUYO_MON = new Recipe().addItem(Items.BROWN_DYE, 7).addItem(Items.BLUE_DYE, 2).addItem(Items.GOLD_NUGGET, 2);
    public static final Recipe KNUT = new Recipe().addItem(Items.LIGHT_BLUE_DYE, 5).addItem(Items.BLUE_DYE, 5).addItem(Items.WHITE_DYE, 5).addItem(Items.SNOWBALL, 4);
    public static final Recipe OVERRIDER = new Recipe().addItem(Items.GOLD_INGOT, 5).addItem(Items.DIAMOND);
    public static final Recipe XPLORER = new Recipe().addItem(Items.EMERALD, 3).addItem(Items.IRON_INGOT, 2);
}
