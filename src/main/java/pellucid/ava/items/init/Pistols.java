package pellucid.ava.items.init;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.guns.AVABulletFilledGun;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.models.p226.P226Model;
import pellucid.ava.misc.renderers.models.python357.Python357Model;
import pellucid.ava.misc.renderers.models.python357.Python357OverRiderModel;
import pellucid.ava.misc.renderers.models.sw1911.SW1911ColtModel;

import java.util.ArrayList;

public class Pistols
{
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */
    public static Item P226_MAGAZINE = null;
    private static final AVAItemGun.Properties P226_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.PISTOL).damage(33).range(39).accuracy(84).stability(90).speed(7).maxAmmo(15).mobility(93 + 6.0F).reloadTime(1.50F)
            .shootSound(AVASounds.P226_SHOOT).reloadSound(AVASounds.P226_RELOAD).initialAccuracy(60.0F).manual().heldAnimation(2).runAnimation(2);

    public static Item P226 = null;

    public static Item SW1911_MAGAZINE = null;
    private static final AVAItemGun.Properties SW1911_COLT_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.PISTOL).damage(41.5F).range(46).accuracy(84.3F).stability(89.7F).speed(6.9F).maxAmmo(8).mobility(86.7F + 6.0F).reloadTime(1.50F)
            .shootSound(AVASounds.SW1911_SHOOT).reloadSound(AVASounds.SW1911_RELOAD).initialAccuracy(60.0F).manual().heldAnimation(2).runAnimation(2);

    public static Item SW1911_COLT = null;

    public static Item PYTHON357_BULLETS = null;
    private static final AVAItemGun.Properties PYTHON357_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.PISTOL).damage(78.5F).range(39).accuracy(84.3F).stability(59.9F).speed(5.56F).maxAmmo(6).mobility(86.7F + 6.0F).reloadTime(3.50F)
            .shootSound(AVASounds.PYTHON357_SHOOT).reloadSound(AVASounds.PYTHON357_RELOAD).initialAccuracy(45.0F).manual().heldAnimation(2).runAnimation(2);

    public static Item PYTHON357 = null;
    public static Item PYTHON357_OVERRIDER = null;

    public static ArrayList<Item> ITEM_PISTOLS = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                P226_MAGAZINE = new Ammo(new Item.Properties().maxDamage(15), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_NUGGET, 3), true).setRegistryName("p226_magazine"),
                P226 = new AVAItemGun(P226_P.magazineType(P226_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 12).addItem(Items.IRON_NUGGET, 3)).setRegistryName("p226"),

                SW1911_MAGAZINE = new Ammo(new Item.Properties().maxDamage(8), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_NUGGET, 3), true).setRegistryName("sw1911_magazine"),
                SW1911_COLT = new AVAItemGun(SW1911_COLT_P.magazineType(SW1911_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 20).addItem(Items.COAL, 5)).setRegistryName("sw1911_colt"),

                PYTHON357_BULLETS = new Ammo(new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT).addItem(Items.GOLD_INGOT).setResultCount(16)).setRegistryName("python357_bullets"),
                PYTHON357 = new AVABulletFilledGun(PYTHON357_P.magazineType(PYTHON357_BULLETS), new Recipe().addItem(Items.IRON_INGOT, 22).addItem(Items.ACACIA_PLANKS, 5)).setRegistryName("python357"),
                PYTHON357_OVERRIDER = new AVABulletFilledGun(PYTHON357_P.skinned((AVAItemGun) PYTHON357), new Recipe().addItem(PYTHON357).mergeIngredients(SharedRecipes.OVERRIDER)).setRegistryName("python357_overrider")
        );
        DistExecutor.runWhenOn(Dist.CLIENT, () -> Pistols::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((AVAItemGun) P226).setCustomModel((origin) -> new Model(origin, P226Model::new));
        ((AVAItemGun) SW1911_COLT).setCustomModel((origin) -> new Model(origin, SW1911ColtModel::new));
        ((AVAItemGun) PYTHON357).setCustomModel((origin) -> new Model(origin, Python357Model::new));
        ((AVAItemGun) PYTHON357_OVERRIDER).setCustomModel((origin) -> new Model(origin, Python357OverRiderModel::new));
    }
}
