package pellucid.ava.items.init;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.entities.shootables.M202RocketEntity;
import pellucid.ava.entities.throwables.GrenadeEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVASpecialWeapon;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.models.gm94.GM94Model;
import pellucid.ava.misc.renderers.models.m202.M202Model;

import java.util.ArrayList;

public class SpecialWeapons
{
    public static Item M202_ROCKET = null;
    public static Item M202 = null;
    public static Item GM94_GRENADE = null;
    public static Item GM94 = null;

    public static ArrayList<Item> ITEM_SPECIAL_WEAPONS = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                M202_ROCKET = new Ammo(new Recipe().addItem(Items.IRON_INGOT, 1).addItem(Items.TNT).addItem(Items.GUNPOWDER, 2)).setRegistryName("m202_rocket"),
                M202 = new AVASpecialWeapon(new AVAItemGun.Properties(AVAWeaponUtil.Classification.SPECIAL_WEAPON).reloadSound(AVASounds.M202_RELOAD).damage(100).range(91).accuracy(82).stability(100).speed(1).maxAmmo(4).mobility(85.0F).reloadTime(1.5F)
                        .shootSound(AVASounds.M202_SHOOT).scopeType(AVAItemGun.ScopeTypes.M202).magazineType(M202_ROCKET), new Recipe().addItem(Items.IRON_INGOT, 48).addItem(Items.IRON_TRAPDOOR, 2).addItem(Items.GUNPOWDER, 4), (world, shooter, spread, weapon) -> new M202RocketEntity(shooter, world, weapon)).setRegistryName("m202"),
                GM94_GRENADE = new Ammo(new Recipe().addItem(Items.IRON_INGOT, 1).addItem(Items.GUNPOWDER, 4)).setRegistryName("gm94_grenade"),
                GM94 = new AVASpecialWeapon(new AVAItemGun.Properties(AVAWeaponUtil.Classification.SPECIAL_WEAPON).reloadSound(AVASounds.GM94_RELOAD).damage(90).range(33).accuracy(62).stability(100).speed(1.5F).maxAmmo(3).mobility(90.0F).reloadTime(1.5F)
                        .shootSound(AVASounds.GM94_SHOOT).scopeType(AVAItemGun.ScopeTypes.GM94).magazineType(GM94_GRENADE), new Recipe().addItem(Items.IRON_INGOT, 48).addItem(Items.GUNPOWDER, 2), (world, shooter, spread, weapon) -> new GrenadeEntity(shooter, world, weapon)).setRegistryName("gm94")
        );
        MiscItems.ITEM_MISCS.addAll(ITEM_SPECIAL_WEAPONS);
        DistExecutor.runWhenOn(Dist.CLIENT, () -> SpecialWeapons::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((AVAItemGun) M202).setCustomModel((origin) -> new Model(origin, M202Model::new));
        ((AVAItemGun) GM94).setCustomModel((origin) -> new Model(origin, GM94Model::new));
    }
}
