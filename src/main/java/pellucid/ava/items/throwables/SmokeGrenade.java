package pellucid.ava.items.throwables;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.ProjectileEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAWeaponUtil;

public class SmokeGrenade extends ThrowableItem
{
    protected final String colour;
    protected final int[] rgb;

    public SmokeGrenade(Recipe dyes, int[] rgb, String colour)
    {
        this(0.75F, 30 * 20, (EntityType<? extends SmokeGrenadeEntity>) AVAEntities.M18, new Recipe().addItem(Items.IRON_NUGGET, 2).addItem(Items.SUGAR, 2).mergeIngredients(dyes), rgb, colour, AVAWeaponUtil.Classification.PROJECTILE);
    }

    public SmokeGrenade(float power, int range, EntityType<? extends SmokeGrenadeEntity> type, Recipe recipe, int[] rgb, String colour, AVAWeaponUtil.Classification classification)
    {
        super(new Item.Properties(), power, range, 0, type, null, recipe, classification);
        this.colour = colour;
        this.rgb = rgb;
    }

    @Override
    protected ProjectileEntity getEntity(World world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss)
    {
        return new SmokeGrenadeEntity((EntityType<? extends SmokeGrenadeEntity>) type, shooter, world, velocity, range, rgb, colour);
    }
}
