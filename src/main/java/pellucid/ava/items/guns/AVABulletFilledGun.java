package pellucid.ava.items.guns;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.items.miscs.Recipe;

public class AVABulletFilledGun extends AVAItemGun
{
    public AVABulletFilledGun(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties, recipe);
    }

    @Override
    public void reload(CompoundNBT compound, LivingEntity shooter, ItemStack stack)
    {
        boolean isPlayer = shooter instanceof PlayerEntity;
        while ((!isPlayer || (getSlotForMagazine((PlayerEntity) shooter) != -1 || ((PlayerEntity) shooter).abilities.isCreativeMode)) && compound.getInt("ammo") < this.maxAmmo && (!this.reloadInteractable || compound.getInt("interact") == 0))
        {
            int need = stack.getDamage();
            int have = this.maxAmmo - need;
            int amount = need;
            if (isPlayer && !((PlayerEntity) shooter).abilities.isCreativeMode)
            {
                int slot = getSlotForMagazine((PlayerEntity) shooter);
                if (slot == -1)
                    break;
                ItemStack mag = ((PlayerEntity) shooter).inventory.getStackInSlot(slot);
                boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                if (isAmmoKit)
                {
                    if (!((AmmoKit) mag.getItem()).refillRequired)
                        amount = need;
                    else
                    {
                        amount = mag.getMaxDamage() - stack.getDamage();
                        if (amount > need)
                        {
                            amount = need;
                            mag.setDamage(mag.getDamage() + amount);
                        }
                    }
                }
                else
                {
                    amount = Math.min(mag.getCount(), need);
                    if (!((PlayerEntity) shooter).isCreative())
                        mag.shrink(amount);
                }
            }
            compound.putInt("ammo", amount + have);
            stack.setDamage(this.maxAmmo - (amount + have));
        }
        compound.putInt("reload", 0);
    }
}
