package pellucid.ava.items.miscs;

import net.minecraft.client.renderer.model.IBakedModel;
import pellucid.ava.misc.renderers.ICustomBakedModelGetter;

public interface ICustomModel
{
    IBakedModel getCustomModel(IBakedModel originalBakedModel);

    void setCustomModel(ICustomBakedModelGetter getter);
}
