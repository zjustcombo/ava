package pellucid.ava.items.miscs;

public interface IHasRecipe
{
    Recipe getRecipe();
}
