package pellucid.ava.items.miscs;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.entities.scanhits.MeleeAttackRaytraceEntity;
import pellucid.ava.misc.AVAItemGroups;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.renderers.ICustomBakedModelGetter;

import java.util.UUID;

public class MeleeWeapon extends Item implements IHasRecipe, IClassification, ICustomModel
{
    protected static final UUID MOVEMENT_SPEED_MODIFIER = UUID.fromString("d1248141-2e53-432d-8a84-1b31d279163e");
    private final Recipe recipe;
    private final float damageL;
    private final float damageR;
    private final int speedL;
    private final int speedR;
    private final float rangeL;
    private final float rangeR;
    @OnlyIn(Dist.CLIENT)
    private ICustomBakedModelGetter bakedModelgetter;
    private final Multimap<String, AttributeModifier> attributeModifiers;

    public MeleeWeapon(Recipe recipe, float damageL, float damageR, float speedL, float speedR, float rangeL, float rangeR, float movementSpeedBonus)
    {
        super(new Properties().group(AVAItemGroups.MAIN).maxStackSize(1));
        this.recipe = recipe;
        this.damageL = damageL / 5F;
        this.damageR = damageR / 5F;
        this.speedL = Math.round(speedL * 20.0F);
        this.speedR = Math.round(speedR * 20.0F);
        this.rangeL = rangeL * 1.7F;
        this.rangeR = rangeR * 1.7F;
        AVAWeaponUtil.Classification.MELEE_WEAPON.addToList(this);
        ImmutableMultimap.Builder<String, AttributeModifier> map = ImmutableMultimap.builder();
        map.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADDITION));
        map.put(SharedMonsterAttributes.MOVEMENT_SPEED.getName(), new AttributeModifier(MOVEMENT_SPEED_MODIFIER, "Weapon modifier", movementSpeedBonus, AttributeModifier.Operation.ADDITION));
        this.attributeModifiers = map.build();
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (!(entityIn instanceof PlayerEntity) || !entityIn.isAlive() || entityIn.world.isRemote())
            return;
        CompoundNBT compound = stack.getOrCreateTag();
        if (isSelected)
            meleeTick(compound, (PlayerEntity) entityIn);
        else
        {
            compound.putInt("meleeL", 0);
            compound.putInt("meleeR", 0);
        }
    }

    private void meleeTick(CompoundNBT compound, PlayerEntity player)
    {
        meleeTick(compound, "L", player);
        meleeTick(compound, "R", player);
    }

    private void meleeTick(CompoundNBT compound, String side, PlayerEntity player)
    {
        String key = "melee" + side;
        boolean left = side.equals("L");
        if (compound.getInt(key) > 0)
        {
            compound.putInt(key, compound.getInt(key) + 1);
            if (compound.getInt(key) >= (left ? speedL : speedR))
                compound.putInt(key, 0);
            else if (compound.getInt(key) == (left ? speedL : speedR) / 2 + 1)
                meleeAttack(player, left);
        }
    }

    public void meleeAttack(PlayerEntity player, boolean left)
    {
        World world = player.world;
        world.addEntity(new MeleeAttackRaytraceEntity(world, player, left ? rangeL : rangeR, left ? damageL : damageR, this));
    }

    public void preMelee(ItemStack stack, boolean left)
    {
        if (canMelee(stack))
            stack.getOrCreateTag().putInt("melee" + (left ? "L" : "R"), 1);
    }

    public boolean canMelee(ItemStack stack)
    {
        return stack.getOrCreateTag().getInt("meleeL") <= 0 && stack.getOrCreateTag().getInt("meleeR") <= 0;
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EquipmentSlotType slot, ItemStack stack)
    {
        return slot == EquipmentSlotType.MAINHAND ? this.attributeModifiers : super.getAttributeModifiers(slot, stack);
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return AVAWeaponUtil.Classification.MELEE_WEAPON;
    }

    @Override
    public Recipe getRecipe()
    {
        return recipe;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void setCustomModel(ICustomBakedModelGetter getter)
    {
        this.bakedModelgetter = getter;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IBakedModel getCustomModel(IBakedModel originalBakedModel)
    {
        return this.bakedModelgetter == null ? null : this.bakedModelgetter.getBakedModel(originalBakedModel);
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player)
    {
        return false;
    }

    @Override
    public UseAction getUseAction(ItemStack stack)
    {
        return UseAction.NONE;
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity)
    {
        return true;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, PlayerEntity player, Entity entity)
    {
        return true;
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged)
    {
        return oldStack.getItem() != newStack.getItem();
    }
}
