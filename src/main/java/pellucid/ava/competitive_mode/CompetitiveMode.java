package pellucid.ava.competitive_mode;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import pellucid.ava.items.miscs.IClassification;
import pellucid.ava.misc.config.AVAServerConfig;

@Mod.EventBusSubscriber
public class CompetitiveMode
{
    @SubscribeEvent
    public static void playerTick(TickEvent.PlayerTickEvent event)
    {
        PlayerEntity player = event.player;
        if (event.phase == TickEvent.Phase.END && isPlayerValid(player) && isModeActive())
        {
            boolean primary = false;
            boolean secondary = false;
            boolean melee = false;
            int projectile = 0;
            boolean special = false;
            PlayerInventory inv = player.inventory;
            for (int i = 0; i < inv.getSizeInventory(); i++)
            {
                ItemStack stack = inv.getStackInSlot(i);
                Item item = inv.getStackInSlot(i).getItem();
                if (isPrimaryWeapon(item))
                {
                    if (!primary)
                        primary = true;
                    else
                        inv.removeStackFromSlot(i);
                }
                else if (isSecondaryWeapon(item))
                {
                    if (!secondary)
                        secondary = true;
                    else
                        inv.removeStackFromSlot(i);
                }
                else if (isMeleeWeapon(item))
                {
                    if (!melee)
                        melee = true;
                    else
                        inv.removeStackFromSlot(i);
                }
                else if (isProjectile(item))
                {
                    if (projectile < 3)
                        projectile = Math.min(3, stack.getCount() + projectile);
                    else
                        inv.removeStackFromSlot(i);
                }
                else if (isSpecialWeapon(item))
                {
                    if (!special)
                        special = true;
                    else
                        inv.removeStackFromSlot(i);
                }
            }
        }
    }

    private static boolean isPrimaryWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 0;
//        return Snipers.ITEM_SNIPERS.contains(item) || Rifles.ITEM_RIFLES.contains(item) || SubmachineGuns.ITEM_SUBMACHINE_GUNS.contains(item);
    }

    private static boolean isSecondaryWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 1;
        //        return Pistols.ITEM_PISTOLS.contains(item);
    }

    private static boolean isMeleeWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 2;
        //        return Pistols.ITEM_PISTOLS.contains(item);
    }

    private static boolean isProjectile(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 3;
//        return Projectiles.ITEM_PROJECTILES.contains(item);
    }

    private static boolean isSpecialWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 4;
//        return (item == SpecialWeapons.M202 || item == SpecialWeapons.GM94 || item == MiscItems.BINOCULAR);
    }

    private static boolean isPlayerValid(PlayerEntity player)
    {
        return player.isAlive() && !player.isSpectator() && !player.isCreative();
    }

    private static boolean isModeActive()
    {
        return AVAServerConfig.isCompetitiveModeActivated();
    }
}
