package pellucid.ava.entities.kits;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntity;
import pellucid.ava.misc.AVASounds;

public abstract class KitEntity extends AVAEntity
{
    public KitEntity(EntityType<? extends KitEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public KitEntity(EntityType<? extends KitEntity> type, World worldIn, double x, double y, double z)
    {
        super(type, worldIn);
        this.setPositionAndRotation(x, y, z, this.rand.nextInt(12) * 30, this.rand.nextInt(12) * 30);
    }

    @Override
    public void tick()
    {
        super.tick();
        //taken from ItemEntity
        Vec3d vec3d = this.getMotion();
        this.setMotion(this.getMotion().add(0.0D, -0.04D, 0.0D));
        if (this.world.isRemote)
            this.noClip = false;
        else
        {
            this.noClip = !this.world.hasNoCollisions(this);
            if (this.noClip)
                this.pushOutOfBlocks(this.getPosX(), (this.getBoundingBox().minY + this.getBoundingBox().maxY) / 2.0D, this.getPosZ());
        }
        if (!this.onGround || horizontalMag(this.getMotion()) > (double)1.0E-5F || (this.ticksExisted + this.getEntityId()) % 4 == 0)
        {
            this.move(MoverType.SELF, this.getMotion());
            if (this.onGround)
                this.setMotion(this.getMotion().mul(1.0D, -0.5D, 1.0D));
        }
        if (!this.world.isRemote) {
            double d0 = this.getMotion().subtract(vec3d).lengthSquared();
            if (d0 > 0.01D)
                this.isAirBorne = true;
        }
        if (this.rangeTravelled > 600)
            this.remove();
    }

    @Override
    public void onCollideWithPlayer(PlayerEntity player)
    {
        if (!world.getEntitiesWithinAABBExcludingEntity(player, player.getBoundingBox()).contains(this))
            return;
        if (player.abilities.isCreativeMode || this.onCollide(player))
        {
            world.playSound(null, this.getPosX(), this.getPosY(), this.getPosZ(), AVASounds.COLLECTS_PICKABLE, SoundCategory.PLAYERS, 1.0F, 1.0F);
            this.remove();
        }
    }

    protected abstract boolean onCollide(PlayerEntity player);
}
