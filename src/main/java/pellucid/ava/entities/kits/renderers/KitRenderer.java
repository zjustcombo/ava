package pellucid.ava.entities.kits.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.entities.kits.AmmoKitEntity;
import pellucid.ava.entities.kits.KitEntity;

@OnlyIn(Dist.CLIENT)
public class KitRenderer extends EntityRenderer<KitEntity>
{
    private static final ResourceLocation AMMO_KIT = new ResourceLocation(AVA.MODID, "textures/entities/ammo_kit.png");
    private static final ResourceLocation FIRST_AID_KIT = new ResourceLocation(AVA.MODID, "textures/entities/first_aid_kit.png");

    private final KitModel kitModel = new KitModel();

    public KitRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn);
    }

    @Override
    public void render(KitEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
        float f = MathHelper.rotLerp(entityIn.prevRotationYaw, entityIn.rotationYaw, partialTicks);
        float f1 = MathHelper.lerp(partialTicks, entityIn.prevRotationPitch, entityIn.rotationPitch);
        this.kitModel.func_225603_a_(0.0F, f, f1);
        kitModel.render(matrixStackIn, bufferIn.getBuffer(this.kitModel.getRenderType(this.getEntityTexture(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
    }

    @Override
    public ResourceLocation getEntityTexture(KitEntity entity)
    {
        return entity instanceof AmmoKitEntity ? AMMO_KIT : FIRST_AID_KIT;
    }
}
