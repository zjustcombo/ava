package pellucid.ava.entities.kits.renderers;
// Made with Blockbench 3.5.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;

public class KitModel extends Model
{
	private final ModelRenderer bone;

	public KitModel()
	{
		super(RenderType::getEntityTranslucent);
		textureWidth = 32;
		textureHeight = 32;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 1.0F, 0.0F);
		bone.setTextureOffset(0, 0).addBox(-3.5F, -2.5F, -1.5F, 7.0F, 5.0F, 3.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bone.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void func_225603_a_(float p_225603_1_, float p_225603_2_, float p_225603_3_) {
		this.bone.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		this.bone.rotateAngleX = p_225603_3_ * ((float)Math.PI / -180F);
	}
}