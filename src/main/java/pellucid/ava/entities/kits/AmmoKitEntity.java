package pellucid.ava.entities.kits;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Ammo;

public class AmmoKitEntity extends KitEntity
{
    public AmmoKitEntity(EntityType<? extends AmmoKitEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public AmmoKitEntity(World worldIn, double x, double y, double z)
    {
        super((EntityType<? extends AmmoKitEntity>) AVAEntities.AMMO_KIT, worldIn, x, y, z);
    }

    @Override
    protected boolean onCollide(PlayerEntity player)
    {
        ItemStack stack = player.getHeldItemMainhand();
        if (stack.getItem() instanceof AVAItemGun)
        {
            if (((AVAItemGun) stack.getItem()).getMagazineType() instanceof Ammo)
                ((Ammo) ((AVAItemGun) stack.getItem()).getMagazineType()).addToInventory(player, 3);
            return true;
        }
        return false;
    }
}
