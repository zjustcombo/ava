package pellucid.ava.entities.kits;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntities;

public class FirstAidKitEntity extends KitEntity
{
    public FirstAidKitEntity(EntityType<? extends FirstAidKitEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public FirstAidKitEntity(World worldIn, double x, double y, double z)
    {
        super((EntityType<? extends FirstAidKitEntity>) AVAEntities.FIRST_AID_KIT, worldIn, x, y, z);
    }

    @Override
    protected boolean onCollide(PlayerEntity player)
    {
        if (player.shouldHeal())
        {
            player.heal(10.0F);
            return true;
        }
        return false;
    }
}
