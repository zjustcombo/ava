package pellucid.ava.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import pellucid.ava.misc.AVAWeaponUtil;

public abstract class BouncingEntity extends ProjectileEntity
{
    protected boolean landed;
    protected SoundEvent collideSound;

    public BouncingEntity(EntityType<? extends BouncingEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public BouncingEntity(EntityType<? extends BouncingEntity> type, LivingEntity shooter, World worldIn, double velocity, int range, SoundEvent collideSound)
    {
        super(type, shooter, worldIn, velocity, range);
        this.collideSound = collideSound;
    }

    @Override
    protected void setDirection()
    {
    }

    @Override
    protected void onImpact(RayTraceResult result)
    {
        if (!world.isRemote())
        {
            if (result.getType() == RayTraceResult.Type.BLOCK)
            {
                if (!landed)
                    world.playSound(null, this.getPosX(), this.getPosY(), this.getPosZ(), collideSound, SoundCategory.PLAYERS, 2.0F, 1.0F);
                BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
                if (AVAWeaponUtil.destroyGlassOnHit(world, ((BlockRayTraceResult) result).getPos()))
                    return;
                Direction.Axis a = blockResult.getFace().getAxis();
                Vec3d m = this.getMotion();
                this.setMotion(new Vec3d(m.getX() * (a == Direction.Axis.X ? -1 : 1), m.getY() * (a == Direction.Axis.Y ? -1 : 1), m.getZ() * (a == Direction.Axis.Z ? -1 : 1)).mul(getBouncingVelocityMultiplier(0.25F)));
            }
        }
        Vec3d m = this.getMotion();
        if (m.getY() <= 0.1F)
        {
            this.setMotion(m.getX(), 0.0D, m.getZ());
            this.landed = true;
        }
        else
            this.landed = false;
    }

    protected Vec3d getBouncingVelocityMultiplier(float factor)
    {
        return new Vec3d(factor, factor, factor);
    }

    @Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putBoolean("landed", landed);
        compound.putString("sound", collideSound.getRegistryName().toString());
    }

    @Override
    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        landed = compound.getBoolean("landed");
        collideSound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(compound.getString("sound")));
    }
    public boolean isLanded()
    {
        return this.landed;
    }
}
