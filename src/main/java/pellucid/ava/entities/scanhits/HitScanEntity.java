package pellucid.ava.entities.scanhits;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.entities.AVAEntity;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

public class HitScanEntity extends AVAEntity
{
    protected LivingEntity shooter;
    protected UUID shooterID;
    protected float range;

    public HitScanEntity(EntityType<?> type, World worldIn)
    {
        super(type, worldIn);
    }

    public HitScanEntity(EntityType<? extends Entity> type, World worldIn, LivingEntity shooter, float range)
    {
        this(type, worldIn);
        this.setPosition(shooter.getPosX(), shooter.getPosY() + shooter.getEyeHeight(), shooter.getPosZ());
        float f = -MathHelper.sin(shooter.rotationYaw * ((float)Math.PI / 180F)) * MathHelper.cos(shooter.rotationPitch * ((float)Math.PI / 180F));
        float f1 = -MathHelper.sin((shooter.rotationPitch) * ((float)Math.PI / 180F));
        float f2 = MathHelper.cos(shooter.rotationYaw * ((float)Math.PI / 180F)) * MathHelper.cos(shooter.rotationPitch * ((float)Math.PI / 180F));
        this.setMotion(new Vec3d(f, f1, f2).normalize().scale(range));
        this.shooter = shooter;
        this.shooterID = shooter.getUniqueID();
        this.range = range;
    }

    @Override
    public void fromMob(LivingEntity target)
    {
        super.fromMob(target);
        double x = target.getPosX() - this.getPosX();
        double y = target.getPosYEye() - this.getPosYEye();
        double z = target.getPosZ() - this.getPosZ();
        Vec3d vec3d = new Vec3d(x, y, z).normalize().scale(range);
        this.setMotion(vec3d);
    }

    protected void setDirection(Vec3d vec3d)
    {
        float f = MathHelper.sqrt(horizontalMag(vec3d));
        this.rotationYaw = (float)(MathHelper.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI));
        this.rotationPitch = (float)(MathHelper.atan2(vec3d.y, f) * (double)(180F / (float)Math.PI));
        this.prevRotationYaw = this.rotationYaw;
        this.prevRotationPitch = this.rotationPitch;
    }

    @Override
    public void tick()
    {
        super.tick();
        setDirection(this.getMotion());
        if (getShooter() == null)
            remove();
        AxisAlignedBB axisalignedbb = this.getBoundingBox().expand(this.getMotion());
        Vec3d from = this.getPositionVec();
        Vec3d to = this.getPositionVec().add(new Vec3d(this.getMotion().getX(), this.getMotion().getY(), this.getMotion().getZ()));
        EntityRayTraceResult entityResult = this.rayTraceEntities(world, this, from, to, axisalignedbb, (entity) -> entity != getShooter() && !entity.isSpectator() && entity.canBeCollidedWith() && entity.isAlive() && canAttack(entity));
        BlockRayTraceResult blockResult = world.rayTraceBlocks(new RayTraceContext(from, to, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, this));
        if (!world.isRemote())
        {
            if (blockResult.getType() != RayTraceResult.Type.MISS && entityResult != null)
                onImpact((from.distanceTo(blockResult.getHitVec()) > from.distanceTo(entityResult.getHitVec())) ? entityResult : blockResult);
            else if (entityResult != null)
                onImpact(entityResult);
            else if (blockResult.getType() != RayTraceResult.Type.MISS)
                onImpact(blockResult);
            else
                onImpact(null);
        }
        this.remove();
    }

    @Override
    public boolean hasNoGravity()
    {
        return true;
    }

    protected void onImpact(@Nullable RayTraceResult result)
    {

    }

    @Nullable
    public EntityRayTraceResult rayTraceEntities(World worldIn, Entity projectile, Vec3d startVec, Vec3d endVec, AxisAlignedBB boundingBox, Predicate<Entity> filter)
    {
        double d0 = Double.MAX_VALUE;
        Entity entity = null;
        Vec3d vec3d = null;
        for(Entity target : worldIn.getEntitiesInAABBexcluding(projectile, boundingBox, filter))
        {
            Optional<Vec3d> optional = target.getBoundingBox().grow(0.1F).rayTrace(startVec, endVec);
            if (optional.isPresent())
            {
                vec3d = optional.get();
                double d1 = startVec.squareDistanceTo(vec3d);
                if (d1 < d0)
                {
                    entity = target;
                    d0 = d1;
                }
            }
        }
        return entity == null ? null : new EntityRayTraceResult(entity, vec3d);
    }

    @Nullable
    public LivingEntity getShooter()
    {
        if ((this.shooter == null || this.shooter.removed) && this.shooterID != null && this.world instanceof ServerWorld)
        {
            Entity entity = ((ServerWorld)this.world).getEntityByUuid(this.shooterID);
            if (entity instanceof LivingEntity)
                this.shooter = (LivingEntity) entity;
            else
                this.shooter = null;
        }
        return this.shooter;
    }

    @Override
    protected void readAdditional(CompoundNBT compound)
    {
        if (this.shooterID != null)
            compound.putUniqueId("owner", this.shooterID);
        compound.putFloat("range", range);
    }

    @Override
    protected void writeAdditional(CompoundNBT compound)
    {
        if (compound.contains("owner"))
            this.shooterID = compound.getUniqueId("owner");
        range = compound.getFloat("range");
    }
}
