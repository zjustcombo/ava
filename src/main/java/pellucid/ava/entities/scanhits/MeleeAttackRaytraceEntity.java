package pellucid.ava.entities.scanhits;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.misc.AVADamageSource;
import pellucid.ava.misc.AVAWeaponUtil;

import javax.annotation.Nullable;

public class MeleeAttackRaytraceEntity extends HitScanEntity
{
    private float damage;
    private Item weapon;
    public MeleeAttackRaytraceEntity(EntityType<? extends MeleeAttackRaytraceEntity> type, World worldIn)
    {
        super(AVAEntities.MELEE_RAYTRACING, worldIn);
    }

    public MeleeAttackRaytraceEntity(World worldIn, LivingEntity shooter, float range, float damage, Item weapon)
    {
        super(AVAEntities.MELEE_RAYTRACING, worldIn, shooter, range);
        this.damage = damage;
        this.weapon = weapon;
    }

    @Override
    protected void onImpact(@Nullable RayTraceResult result)
    {
        if (result != null)
        {
            Vec3d vec = result.getHitVec();
            if (result.getType() == RayTraceResult.Type.ENTITY)
            {
                Entity target = ((EntityRayTraceResult) result).getEntity();
                float multiplier = 1;
                if (Math.abs(target.getPosYEye() - vec.getY()) <= 0.3125F)
                    multiplier *= 2.0F;
                AVAWeaponUtil.attackEntityDependAllyDamage(target, AVADamageSource.causeDamageDirect(getShooter(), weapon), damage * multiplier);
            }
            else if (result.getType() == RayTraceResult.Type.BLOCK)
            {
                SoundEvent sound = world.getBlockState(((BlockRayTraceResult) result).getPos()).getSoundType().getBreakSound();
                if (!AVAWeaponUtil.destroyGlassOnHit(world, ((BlockRayTraceResult) result).getPos()))
                    playSound(sound, SoundCategory.BLOCKS, vec.getX(), vec.getY(), vec.getZ(), 1.25F, 1.25F);
            }
        }
    }

    @Override
    protected void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putFloat("damage", damage);
        compound.putString("weapon", weapon.getRegistryName().toString());
    }

    @Override
    protected void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        damage = compound.getFloat("damage");
        weapon = ForgeRegistries.ITEMS.getValue(new ResourceLocation(compound.getString("weapon")));
    }
}
