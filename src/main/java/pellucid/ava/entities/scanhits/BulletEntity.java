package pellucid.ava.entities.scanhits;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPartEntity;
import net.minecraft.entity.item.EnderCrystalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.registries.ForgeRegistries;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.AVADamageSource;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.cap.IWorldData;
import pellucid.ava.misc.cap.WorldData;

import javax.annotation.Nullable;

public class BulletEntity extends HitScanEntity
{
    private float damage;
    private AVAItemGun weapon = null;
    protected float spread;

    public BulletEntity(EntityType<? extends Entity> type, World worldIn)
    {
        super(AVAEntities.BULLET, worldIn);
    }

    public BulletEntity(World worldIn, LivingEntity shooter, AVAItemGun weapon, int range, float damage, float spread)
    {
        super(AVAEntities.BULLET, worldIn, shooter, (int) (range * 1.5F));
        this.spread = spread;
        spread();
        this.damage = damage;
        this.weapon = weapon;
    }

    @Override
    public void fromMob(LivingEntity target)
    {
        super.fromMob(target);
        spread();
    }

    protected void spread()
    {
        double x = (-spread + this.rand.nextFloat() * (spread + spread)) * 4.0F;
        double y = (-spread + this.rand.nextFloat() * (spread + spread)) * 4.0F;
        double z = (-spread + this.rand.nextFloat() * (spread + spread)) * 4.0F;
        this.setMotion(this.getMotion().add(x, y, z));
    }

    @Override
    protected void onImpact(@Nullable RayTraceResult result)
    {
        if (result == null)
            return;
        Vec3d vec = result.getHitVec();
        double x = getPosX() - vec.x;
        double y = getPosY() - vec.y;
        double z = getPosZ() - vec.z;
        int count = (int) (Math.round((Math.max(Math.max(Math.abs(x), Math.abs(y)), Math.abs(z)))) / 1.5F);
        for (int i=1;i<count&&i<10;i++)
            playSound(AVASounds.BULLET_FLIES_BY, SoundCategory.PLAYERS, vec.x + x / count * i, vec.y / count * i, vec.z + z / count * i, 1.35F, 1F + rand.nextFloat());
        if (result.getType() == RayTraceResult.Type.BLOCK)
        {
            BlockPos pos = ((BlockRayTraceResult) result).getPos();
            BlockState state = world.getBlockState(pos);
            Material material = state.getMaterial();
            if (material == Material.GLASS)
                if (WorldData.getCap(world).isGlassDestroyable())
                    world.destroyBlock(pos, false);
            ServerWorld server = (ServerWorld) this.world;
            SoundEvent sound = world.getBlockState(((BlockRayTraceResult) result).getPos()).getSoundType().getBreakSound();
            playSound(sound, SoundCategory.BLOCKS, vec.getX(), vec.getY(), vec.getZ(), 1.25F, 1.25F);
            server.spawnParticle(ParticleTypes.SMOKE, vec.getX(), vec.getY(), vec.getZ(), 5, 0.0F, 0.0F, 0.0F, 0.0F);
            server.spawnParticle(new BlockParticleData(ParticleTypes.BLOCK, state), vec.getX(), vec.getY(), vec.getZ(), 5, 0.0F, 0.0F, 0.0F, 0.0F);
        }
        else if (result.getType() == RayTraceResult.Type.ENTITY)
        {
            Entity target = ((EntityRayTraceResult) result).getEntity();
            boolean isEnderCrystal = target instanceof EnderCrystalEntity && !fromMob;
            boolean isDragonParts = target instanceof EnderDragonPartEntity && !fromMob;
            if (target instanceof LivingEntity || isDragonParts)
            {
                if (getShooter() == null)
                    return;
                if (target == getShooter())
                    return;
                target.hurtResistantTime = 0;
                float multiplier = Math.max(0.6F, 1.0F - (float) target.getPositionVec().distanceTo(getPositionVec()) / (float) range);
                if (Math.abs(target.getPosYEye() - vec.getY()) <= 0.3125F)
                    multiplier *= 2.0F;
                if (target instanceof PlayerEntity)
                {
                    multiplier *= 0.85F;
                    multiplier = multiplier + 0.05F + this.rand.nextFloat() * 0.05F;
                    IWorldData capability = WorldData.getCap(target.getEntityWorld());
                    if (!capability.isFriendlyFireAllowed() && target.isOnSameTeam(getShooter()))
                        return;
                    else if (capability.isFriendlyFireReduced())
                        multiplier *= 0.2F;
                }
                if (target instanceof LivingEntity && ((LivingEntity) target).isActiveItemStackBlocking())
                    ((LivingEntity) target).resetActiveHand();
                AVAWeaponUtil.attackEntity(target, AVADamageSource.causeBulletDamageIndirect(getShooter(), this, weapon), damage, multiplier);
                ((ServerWorld) this.world).spawnParticle(new BlockParticleData(ParticleTypes.BLOCK, Blocks.REDSTONE_BLOCK.getDefaultState()), vec.getX(), vec.getY(), vec.getZ(), 5, 0.0F, 0.0F, 0.0F, 0.0F);
            }
            if (isEnderCrystal)
                target.attackEntityFrom(AVADamageSource.causeBulletDamageIndirect(getShooter(), this, weapon), 1.0F);
        }
        BlockRayTraceResult fluidResult = world.rayTraceBlocks(new RayTraceContext(getPositionVec(), vec, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY, this));
        if (fluidResult.getType() != RayTraceResult.Type.MISS && world.getFluidState(fluidResult.getPos()) == Fluids.WATER.getDefaultState())
        {
            vec = fluidResult.getHitVec();
            ((ServerWorld) this.world).spawnParticle(ParticleTypes.BUBBLE, vec.getX(), vec.getY(), vec.getZ(), 5, 0.0F, 0.0F, 0.0F, 0.125F);
            ((ServerWorld) this.world).spawnParticle(new BlockParticleData(ParticleTypes.BLOCK, Blocks.WATER.getDefaultState()), vec.getX(), vec.getY(), vec.getZ(), 5, 0.0F, 0.0F, 0.0F, 0.125F);
        }
    }

    @Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putFloat("damage", this.damage);
        if (this.weapon != null)
            compound.putString("weapon", this.weapon.getRegistryName().toString());
        compound.putFloat("spread", spread);
    }


    @Override
    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        this.damage = compound.getInt("damage");
        if (compound.contains("weapon"))
        {
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(compound.getString("weapon")));
            this.weapon = item == Items.AIR ? null : (AVAItemGun) item;
        }
        this.spread = compound.getFloat("spread");
    }
}

