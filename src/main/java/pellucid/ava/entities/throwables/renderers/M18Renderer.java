package pellucid.ava.entities.throwables.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.misc.renderers.SmokeRenderer;

@OnlyIn(Dist.CLIENT)
public class M18Renderer<S extends SmokeGrenadeEntity> extends ProjectileRenderer<S>
{
    private static final ResourceLocation GREY = new ResourceLocation(AVA.MODID, "textures/entities/m18_grey.png");
    private static final ResourceLocation PURPLE = new ResourceLocation(AVA.MODID, "textures/entities/m18_purple.png");
    private static final ResourceLocation LIME = new ResourceLocation(AVA.MODID, "textures/entities/m18_toxic.png");

    public M18Renderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn, new CylinderGrenadeModel(), null);
    }

    @Override
    public void render(S entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
        SmokeRenderer.render(entityIn, matrixStackIn);
    }

    @Override
    public ResourceLocation getEntityTexture(S entity)
    {
        switch (entity.colour)
        {
            case "grey":
            default:
                return GREY;
            case "purple":
                return PURPLE;
            case "lime":
                return LIME;
        }
    }
}
