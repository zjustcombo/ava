package pellucid.ava.entities.throwables;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.misc.AVASounds;

public class GrenadeEntity extends HandGrenadeEntity
{
    public GrenadeEntity(EntityType<? extends HandGrenadeEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public GrenadeEntity(LivingEntity shooter, World worldIn, Item weapon)
    {
        super((EntityType<? extends HandGrenadeEntity>) AVAEntities.GRENADE, shooter, worldIn, 0.9F, weapon, false, 90 / 5, 0, 40, 5, AVASounds.GENERIC_GRENADE_EXPLODE);
    }

    @Override
    protected void explode()
    {
        explode(
                (entity, distance) -> {},
                (serverWorld, x, z) -> {
                    serverWorld.spawnParticle(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 1, 0.0F, 0.0F, 0.0F, 0.125F);
                    serverWorld.spawnParticle(ParticleTypes.FLAME, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 3, 0.0F, 0.0F, 0.0F, 0.125F);
                }
        );
    }

    @Override
    protected Vec3d getBouncingVelocityMultiplier(float factor)
    {
        return new Vec3d(0.1F, 0.25F, 0.1F);
    }

    @Override
    protected double getGravityVelocity()
    {
        return 0.0135D;
    }
}
