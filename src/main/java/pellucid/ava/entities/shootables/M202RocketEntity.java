package pellucid.ava.entities.shootables;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.misc.AVASounds;

public class M202RocketEntity extends HandGrenadeEntity
{
    public M202RocketEntity(EntityType<? extends M202RocketEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public M202RocketEntity(LivingEntity shooter, World worldIn, Item weapon)
    {
        super((EntityType<? extends HandGrenadeEntity>) AVAEntities.ROCKET, shooter, worldIn, 1.2F, weapon, true, 100 / 5, 0, 140, 6, AVASounds.ROCKET_TRAVEL, AVASounds.ROCKET_EXPLODE);
    }

    @Override
    protected void setDirection()
    {
        Vec3d vec3d = getMotion();
        float f = MathHelper.sqrt(horizontalMag(vec3d));
        this.rotationYaw = (float)(MathHelper.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI));
        this.rotationPitch = (float)(MathHelper.atan2(vec3d.y, f) * (double)(180F / (float)Math.PI));
        this.prevRotationYaw = this.rotationYaw;
        this.prevRotationPitch = this.rotationPitch;
    }

    @Override
    protected void move()
    {
        move(false);
    }

    @Override
    protected void move(boolean rotates)
    {
        super.move(rotates);
        if (world.getGameTime() % 2L == 0)
        {
            world.playMovingSound(null, this, AVASounds.ROCKET_TRAVEL, SoundCategory.PLAYERS, 1.0F, 1.0F);
            if (world instanceof ServerWorld)
            {
                ((ServerWorld) world).spawnParticle(ParticleTypes.FLAME, getPosX(), getPosY(), getPosZ(), 2, (this.rand.nextFloat() - 0.5F) / 2, (this.rand.nextFloat() - 0.5F) / 2, (this.rand.nextFloat() - 0.5F) / 2, 0.015F);
                ((ServerWorld) world).spawnParticle(ParticleTypes.SMOKE, getPosX(), getPosY(), getPosZ(), 2, (this.rand.nextFloat() - 0.5F) / 2, (this.rand.nextFloat() - 0.5F) / 2, (this.rand.nextFloat() - 0.5F) / 2, 0.005F);
            }
        }
    }

    @Override
    protected void explode()
    {
        explode(
                (entity, distance) -> {},
                (serverWorld, x, z) -> {
                    serverWorld.spawnParticle(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.25F);
                    serverWorld.spawnParticle(ParticleTypes.FLAME, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.25F);
                }
        );
    }

    @Override
    protected double getGravityVelocity()
    {
        return 0.00125D;
    }
}
