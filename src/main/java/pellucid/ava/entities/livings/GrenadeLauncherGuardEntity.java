package pellucid.ava.entities.livings;

import com.google.common.collect.ImmutableMap;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.SpecialWeapons;

public class GrenadeLauncherGuardEntity extends GunGuardEntity
{
    public GrenadeLauncherGuardEntity(EntityType<? extends GrenadeLauncherGuardEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected int getAttackInterval()
    {
        return 60;
    }

    @Override
    protected ItemStack getWeapon()
    {
        ItemStack weapon = new ItemStack(SpecialWeapons.GM94);
        AVAItemGun gun = (AVAItemGun) weapon.getItem();
        gun.reload(gun.initTags(weapon), this, weapon);
        return weapon;
    }

    private static final ImmutableMap<Item, Integer> DROPS = ImmutableMap.of(SpecialWeapons.GM94_GRENADE, 7);
    @Override
    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return DROPS;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void modifyItemModel(Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        super.modifyItemModel(rotation, translation, scale);
        translation.add(0.0F, -13.0F, 0.0F);
    }
}
