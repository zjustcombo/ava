package pellucid.ava.entities.livings;

import com.google.common.collect.ImmutableMap;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.throwables.ToxicSmokeGrenade;

public class ToxicSmokeGuardEntity extends RangedGuardEntity
{
    public ToxicSmokeGuardEntity(EntityType<? extends CreatureEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected int getAttackInterval()
    {
        return 120;
    }

    @Override
    protected ItemStack getWeapon()
    {
        return new ItemStack(Projectiles.M18_TOXIC);
    }

    @Override
    public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
    {
        if (!world.isRemote() && world instanceof ServerWorld)
        {
            ItemStack stack = getHeldItemMainhand();
            if (stack.getItem() instanceof ToxicSmokeGrenade)
                ((ToxicSmokeGrenade) stack.getItem()).toss((ServerWorld) world, this, target, stack, false);
        }
    }

    private static final ImmutableMap<Item, Integer> DROPS = ImmutableMap.of(Projectiles.M18_TOXIC, 4);
    @Override
    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return DROPS;
    }

    @Override
    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.randomColourFrom(ColourTypes.YELLOW, ColourTypes.RED);
    }
}
