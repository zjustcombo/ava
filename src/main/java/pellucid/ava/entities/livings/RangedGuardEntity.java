package pellucid.ava.entities.livings;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.RandomWalkingGoal;
import net.minecraft.entity.ai.goal.RangedAttackGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public abstract class RangedGuardEntity extends AVAHostileEntity implements IRangedAttackMob
{
    public RangedGuardEntity(EntityType<? extends CreatureEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected void registerGoals()
    {
        goalSelector.addGoal(1, new RangedAttackGoal(this, 0.5F, getAttackInterval(), 70));
        goalSelector.addGoal(2, new RandomWalkingGoal(this, 0.35F, 5));
        targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, 2, false, false, (entity) -> entity instanceof PlayerEntity && entity.isAlive() && !((PlayerEntity) entity).isCreative()));
    }

    @Override
    public void registerAttributes()
    {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(25.0F);
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.4D);
        getAttribute(SharedMonsterAttributes.ATTACK_KNOCKBACK).setBaseValue(0.0F);
        getAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(1.0F);
        getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(5.0F);
        getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(70.0F);
    }

    protected int getAttackInterval()
    {
        return 1;
    }
}
