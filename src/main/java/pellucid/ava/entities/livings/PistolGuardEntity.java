package pellucid.ava.entities.livings;

import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.Pistols;

public class PistolGuardEntity extends GunGuardEntity
{
    public PistolGuardEntity(EntityType<? extends PistolGuardEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public float getSpread(float spread)
    {
        return 2.25F;
    }

    @Override
    protected ItemStack getWeapon()
    {
        ItemStack weapon = new ItemStack(Pistols.P226);
        AVAItemGun gun = (AVAItemGun) weapon.getItem();
        gun.reload(gun.initTags(weapon), this, weapon);
        return weapon;
    }
}
