package pellucid.ava.entities.livings;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.RandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.world.World;

public class MeleeGuardEntity extends AVAHostileEntity
{
    public MeleeGuardEntity(EntityType<? extends AVAHostileEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected void registerGoals()
    {
        goalSelector.addGoal(1, new MeleeAttackGoal(this, 0.9F, true));
        goalSelector.addGoal(2, new MoveTowardsTargetGoal(this, 0.9F, 50));
        goalSelector.addGoal(3, new RandomWalkingGoal(this, 0.25F, 30));
        targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, 2, false, false, (entity) -> entity instanceof PlayerEntity && entity.isAlive() && !((PlayerEntity) entity).isCreative()));
    }

    @Override
    public void registerAttributes()
    {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0F);
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.4D);
        getAttribute(SharedMonsterAttributes.ATTACK_KNOCKBACK).setBaseValue(0.0F);
        getAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(1.0F);
        getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(5.0F);
        getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(50.0F);
    }

    @Override
    protected ItemStack getWeapon()
    {
        return new ItemStack(Items.STICK);
    }

    @Override
    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.BLUE;
    }
}
