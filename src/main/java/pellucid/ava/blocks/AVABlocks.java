package pellucid.ava.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.Rarity;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.blocks.boost_block.BoostBlock;
import pellucid.ava.blocks.colouring_table.GunColouringTable;
import pellucid.ava.blocks.crafting_table.GunCraftingTable;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.miscs.IHasRecipe;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAItemGroups;

public class AVABlocks
{
    public static Block GUN_CRAFTING_TABLE = new GunCraftingTable(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0F, 3.0F)).setRegistryName("gun_crafting_table");
    public static Block GUN_COLOURING_TABLE = new GunColouringTable(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0F, 3.0F)).setRegistryName("gun_colouring_table");

    public static Block AMMO_KIT_SUPPLIER = new AmmoKitSupplier().setRegistryName("ammo_kit_supplier");

    public static Block ATTACK_DAMAGE_BOOST_BLOCK = new BoostBlock().setRegistryName("attack_damage_boost_block");
    public static Block HEALTH_BOOST_BLOCK = new BoostBlock().setRegistryName("health_boost_block");

    public static void registerAllItems(IForgeRegistry<Item> registry)
    {
        Item kitSuppier = createBlockItemWithRecipe("ammo_kit_supplier", AMMO_KIT_SUPPLIER, new Item.Properties().group(AVAItemGroups.MAP_CREATION).rarity(Rarity.EPIC), new Recipe().addItem(Items.DIAMOND_BLOCK, 15).addItem(Items.IRON_BLOCK, 10).addItem(Items.GUNPOWDER, 64));
        registry.registerAll(
                new BlockItem(GUN_CRAFTING_TABLE, new Item.Properties().group(AVAItemGroups.MAIN)).setRegistryName("gun_crafting_table"),
                new BlockItem(GUN_COLOURING_TABLE, new Item.Properties().group(AVAItemGroups.MAIN)).setRegistryName("gun_colouring_table"),

                kitSuppier,

                new BlockItem(ATTACK_DAMAGE_BOOST_BLOCK, new Item.Properties().group(AVAItemGroups.MAP_CREATION).rarity(Rarity.EPIC)).setRegistryName("attack_damage_boost_block"),
                new BlockItem(HEALTH_BOOST_BLOCK , new Item.Properties().group(AVAItemGroups.MAP_CREATION).rarity(Rarity.EPIC)).setRegistryName("health_boost_block")
        );
        MiscItems.ITEM_MISCS.add(kitSuppier);
    }

    private static Item createBlockItemWithRecipe(String name, Block block, Item.Properties properties, Recipe recipe)
    {
        return new BlockItemWithRecipe(block, properties, recipe).setRegistryName(name);
    }

    static class BlockItemWithRecipe extends BlockItem implements IHasRecipe
    {
        private final Recipe recipe;

        public BlockItemWithRecipe(Block blockIn, Properties builder, Recipe recipe)
        {
            super(blockIn, builder);
            this.recipe = recipe;
        }

        @Override
        public Recipe getRecipe()
        {
            return recipe;
        }
    }

    public static void registerAllBlocks(IForgeRegistry<Block> registry)
    {
        registry.registerAll(
                GUN_CRAFTING_TABLE = new GunCraftingTable(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0F, 3.0F)).setRegistryName("gun_crafting_table"),
                GUN_COLOURING_TABLE = new GunColouringTable(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0F, 3.0F)).setRegistryName("gun_colouring_table"),

                AMMO_KIT_SUPPLIER = new AmmoKitSupplier().setRegistryName("ammo_kit_supplier"),

//                ATTACK_DAMAGE_BOOST_BLOCK = new BoostBlock().setRegistryName("attack_damage_boost_block"),
//                HEALTH_BOOST_BLOCK = new BoostBlock().setRegistryName("health_boost_block")
                ATTACK_DAMAGE_BOOST_BLOCK,
                HEALTH_BOOST_BLOCK
        );
    }
}
