package pellucid.ava.blocks.colouring_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.AbstractButton;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.PaintMessage;

import java.util.ArrayList;

@OnlyIn(Dist.CLIENT)
public class GunColouringGUI extends ContainerScreen<GunColouringTableContainer>
{
    private static final ResourceLocation GUN_CRAFTING_GUI = new ResourceLocation("ava:textures/gui/gun_colouring_table.png");
    private int current_index = 0;
    private int current_shown_index = 0;
    private ArrayList<AVAItemGun> gunsInv = new ArrayList<>();
    private ArrayList<AVAItemGun> skins = new ArrayList<>();

    public GunColouringGUI(GunColouringTableContainer screenContainer, PlayerInventory inv, ITextComponent titleIn)
    {
        super(screenContainer, inv, titleIn);
        this.xSize = 230;
        this.ySize = 219;
    }

    protected void clearAll()
    {
        this.buttons.clear();
        this.children.clear();
    }

    @Override
    protected void init()
    {
        super.init();
        clearAll();
        this.addButton(new SelectButton(this.guiLeft + 60, this.guiTop + 45, true, true));
        this.addButton(new SelectButton(this.guiLeft + 162, this.guiTop + 45, false, true));
        this.addButton(new SelectButton(this.guiLeft + 46, this.guiTop + 90, true, false));
        this.addButton(new SelectButton(this.guiLeft + 88, this.guiTop + 90, false, false));
        this.addButton(new PaintButton(this.guiLeft + 99, this.guiTop + 84));
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        if (this.getShownGun(this.current_shown_index) != Items.AIR)
        {
            AVAItemGun gun = (AVAItemGun) this.getShownGun(this.current_shown_index);
            RenderSystem.pushMatrix();
            RenderSystem.scalef(1.75F, 1.75F, 1.75F);
            drawCenteredString(this.font, new TranslationTextComponent(gun.getTranslationKey()).getString(), 67, 9, 150);
            RenderSystem.popMatrix();
        }
        this.update();
        if (this.gunsInv.size() <= this.current_index)
            return;
        this.itemRenderer.zLevel = 100.0F;
        AVAItemGun selectedGun = this.gunsInv.get(this.current_index);
        this.itemRenderer.renderItemIntoGUI(new ItemStack(selectedGun), 64, 93);
        RenderSystem.pushMatrix();
        RenderSystem.scalef(0.85F, 0.85F, 0.85F);
        drawCenteredString(this.font, new TranslationTextComponent(selectedGun.getTranslationKey()).getString(), 85, 132, 150);
        RenderSystem.popMatrix();
        if (this.getShownGun(this.current_shown_index - 1) != Items.AIR)
            renderItemShown(this.getShownGun(this.current_shown_index - 1), 14, 44, false);
        if (this.getShownGun(this.current_shown_index) != Items.AIR)
        {
            AVAItemGun gun = (AVAItemGun) this.getShownGun(this.current_shown_index);
            renderItemShown(gun, 74, 12, true);
            if (!gun.isMaster())
            {
                Recipe recipe = gun.getRecipe();
                int f = 124;
                for (Item ingredient : recipe.getIngredients())
                {
                    renderRecipeItem(recipe, ingredient, f, 93);
                    f += 18;
                }
            }
            else
                this.itemRenderer.renderItemIntoGUI(new ItemStack(gun), 124, 93);
        }
        if (this.getShownGun(this.current_shown_index + 1) != Items.AIR)
            renderItemShown(this.getShownGun(this.current_shown_index + 1), 166, 44, false);
        this.itemRenderer.zLevel = 0.0F;
        for(Widget widget : this.buttons)
            if (widget.isHovered())
            {
                widget.renderToolTip(mouseX - this.guiLeft, mouseY - this.guiTop);
                break;
            }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        if (this.minecraft == null)
            return;
        this.minecraft.getTextureManager().bindTexture(GUN_CRAFTING_GUI);
        int x = (this.width - this.xSize) / 2;
        int y = (this.height - this.ySize) / 2;
        this.blit(x, y, 0, 0, this.xSize, this.ySize);
    }

    protected void renderRecipeItem(Recipe recipe, Item item, int x, int z)
    {
        ItemStack stack = new ItemStack(item);
        stack.setCount(recipe.getCount(item));
        this.itemRenderer.renderItemIntoGUI(stack, x, z);
        this.itemRenderer.renderItemOverlayIntoGUI(this.font, stack, x, z, null);
    }

    protected void renderItemShown(Item item, int x, int z, boolean primary)
    {
        ItemStack stack = new ItemStack(item);
        double D = primary ? 5.75D : 3.45D;
        x /= D;
        z /= D;
        RenderSystem.pushMatrix();
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.scaled(D, D, 1.0D);
        this.itemRenderer.renderItemIntoGUI(stack, x, z);
        RenderSystem.popMatrix();
    }

    protected Item getShownGun(int index)
    {
        return index < 0 ? Items.AIR : this.skins.size() > index ? this.skins.get(index) : Items.AIR;
    }

    protected void update()
    {
        this.gunsInv.clear();
        for (int slot=0;slot<this.playerInventory.getSizeInventory();slot++)
            if (this.playerInventory.getStackInSlot(slot).getItem() instanceof AVAItemGun)
                this.gunsInv.add((AVAItemGun) this.playerInventory.getStackInSlot(slot).getItem());
        this.skins.clear();
        if (this.isIndexAvailable(this.current_index, false))
            this.skins = new ArrayList<>(getSkinsFromGun(this.gunsInv.get(this.current_index)));
    }

    protected ArrayList<AVAItemGun> getSkinsFromGun(AVAItemGun gun)
    {
        ArrayList<AVAItemGun> guns = gun.getSubGuns();
        return isOrigin(gun) ? guns : gun.getMaster().getSubGuns();
    }

    protected boolean isOrigin(AVAItemGun gun)
    {
        return !gun.getSubGuns().isEmpty();
    }

    protected void setGunShown(int index)
    {
//        index = index < 0 ? 0 : this.skins.size() <= index ? this.skins.size() - 1 : index;
//        if (index >= 0)
        if (isIndexAvailable(index, true))
            this.current_shown_index = index;
    }

    protected void setGunSelected(int index)
    {
//        index = index < 0 ? 0 : this.gunsInv.size() <= index ? this.gunsInv.size() - 1 : index;
//        if (index >= 0)
        if (isIndexAvailable(index, false))
        {
            this.current_shown_index = 0;
            this.current_index = index;
        }
    }

    protected boolean isIndexAvailable(int index, boolean forSkin)
    {
        return (index >= 0) && (forSkin ? this.skins.size() > index : this.gunsInv.size() > index);
    }

    protected boolean canPaint()
    {
        if (isIndexAvailable(this.current_shown_index, true))
        {
            AVAItemGun selected = this.skins.get(this.current_shown_index);
            if (this.isOrigin(selected) || this.playerInventory.player.isCreative())
                return true;
            else
                return selected.getRecipe().canCraft(this.playerInventory.player, selected);
        }
        return false;
    }

    protected void onPaint()
    {
        if (canPaint())
        {
            if (!this.isIndexAvailable(this.current_shown_index, true) || !this.isIndexAvailable(this.current_index, false))
                return;
            AVAItemGun from = this.gunsInv.get(this.current_index);
            AVAItemGun to = this.skins.get(this.current_shown_index);
            AVAPackets.INSTANCE.sendToServer(new PaintMessage(from, to));
        }
    }

    @OnlyIn(Dist.CLIENT)
    class SelectButton extends AbstractButton
    {
        private boolean left;
        private boolean forSkin;
        private boolean isAvailable;

        public SelectButton(int xIn, int yIn, boolean left, boolean forSkin)
        {
            super(xIn, yIn, 10, 22,"");
            this.left = left;
            this.forSkin = forSkin;
        }

        @Override
        public void onPress()
        {
            GunColouringGUI gui = GunColouringGUI.this;
            if (this.isAvailable)
            {
                if (this.forSkin)
                {
                    if (this.left)
//                        if (gui.isIndexAvailable(gui.current_shown_index - 1, true))
                        gui.setGunShown(gui.current_shown_index - 1);
                    else
//                        if (gui.isIndexAvailable(gui.current_shown_index + 1, true))
                        gui.setGunShown(gui.current_shown_index + 1);
                }
                else
                {
                    if (this.left)
//                        if (gui.isIndexAvailable(gui.current_shown_index - 1, false))
                        gui.setGunSelected(gui.current_index - 1);
                    else
//                        if (gui.isIndexAvailable(gui.current_shown_index + 1, false))
                        gui.setGunSelected(gui.current_index + 1);
                }
            }
        }

        @Override
        public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_)
        {
            this.isAvailable = GunColouringGUI.this.isIndexAvailable(getIndex(), this.forSkin);
            Minecraft.getInstance().getTextureManager().bindTexture(GUN_CRAFTING_GUI);
            int x = this.left ? 96 : 66;
            if (this.isAvailable)
            {
                x += this.width;
                if (this.isHovered())
                    x += this.width;
            }
            this.blit(this.x, this.y, x, 219, this.width, this.height);
        }

        private int getIndex()
        {
            GunColouringGUI gui = GunColouringGUI.this;
            return (this.forSkin ? gui.current_shown_index : gui.current_index) + (this.left ? -1 : + 1) ;
        }
    }

    @OnlyIn(Dist.CLIENT)
    class PaintButton extends AbstractButton
    {
        private boolean selected;

        public PaintButton(int xIn, int yIn)
        {
            super(xIn, yIn, 22, 9, "");
        }

        @Override
        public void onPress()
        {
            if (this.active)
            {
                GunColouringGUI.this.onPaint();
                this.selected = true;
            }
        }

        @Override
        public void onRelease(double p_onRelease_1_, double p_onRelease_3_)
        {
            if (this.active)
                this.selected = false;
        }

        @Override
        public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_)
        {
            this.active = GunColouringGUI.this.canPaint();
            Minecraft.getInstance().getTextureManager().bindTexture(GUN_CRAFTING_GUI);
            int x = 0;
            if (this.active && this.isHovered())
                x += this.width;
            if (this.active && this.selected)
                x += this.width;
            if (!this.active)
                x += this.width * 2;
            this.blit(this.x, this.y, x, 219, this.width, this.height);
            drawCenteredString(GunColouringGUI.this.font, "Paint", this.x + this.width / 2, this.y, this.active ? 54783 : 16711680);
        }
    }
}
